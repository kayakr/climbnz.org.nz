<?php

/**
 * @file
 * climbnz_audit.features.inc
 */

/**
 * Implements hook_views_api().
 */
function climbnz_audit_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
