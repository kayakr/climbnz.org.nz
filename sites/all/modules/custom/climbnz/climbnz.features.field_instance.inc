<?php

/**
 * @file
 * climbnz.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function climbnz_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-page-field_open_api_file'.
  $field_instances['node-page-field_open_api_file'] = array(
    'bundle' => 'page',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'swagger_ui_formatter',
        'settings' => array(
          'doc_expansion' => 'full',
          'show_top_bar' => 1,
          'sort_tags_by_name' => 0,
          'validator' => 'default',
          'validator_url' => '',
        ),
        'type' => 'swagger_ui_formatter_swagger_ui',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_open_api_file',
    'label' => 'Open API File',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'openapi',
      'file_extensions' => 'yml json',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 'document',
          'image' => 0,
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 0,
          'upload' => 'upload',
        ),
      ),
      'type' => 'media_generic',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Open API File');

  return $field_instances;
}
