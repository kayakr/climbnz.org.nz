<?php
/**
 * @file
 * Add block for footer links to work-around not being able to use menu UI due to huge menu...
 *
 */

include_once 'climbnz.features.inc';

/**
 * Define handler for homepage.
 *
 * Implements hook_menu().
 */
function climbnz_menu() {
  $items['home'] = array(
    'title' => 'ClimbNZ',
    'page callback' => 'climbnz_home',
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  );

  // View help topic.
  $items['faq'] = array(
    'page callback' => 'advanced_help_topic_page',
    'page arguments' => array('climbnz', 'faq'),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Return empty content as homepage controlled via context and blocks.
 *
 * @return string
 */
function climbnz_home() {
  return '<!--{climbnz homepage}-->';
}

/**
 * Define blocks show ClimbNZ Introduction (on homepage), and to list footer links.
 *
 * Implements hook_block_info().
 */
function climbnz_block_info() {
  $blocks['climbnz_d7_eol'] = array(
    'info' => t('ClimbNZ D7 EOL'),
    'cache' => DRUPAL_NO_CACHE,
  );

  $blocks['climbnz_intro'] = array(
    'info' => t('ClimbNZ Introduction'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );

  $blocks['climbnz_intro_migration'] = array(
    'info' => t('ClimbNZ Migration'),
    'cache' => DRUPAL_NO_CACHE, // during development
  );

  $blocks['climbnz_footer_links'] = array(
    'info' => t('ClimbNZ Footer links'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function climbnz_block_view($delta = '') {
  global $user;
  $block = array();

  switch ($delta) {
    case 'climbnz_d7_eol':
      $block['subject'] = t('ClimbNZ is getting an upgrade');
      $block['content'] = '<p><a href="https://alpineclub.org.nz/">NZAC</a>, supported by the <a href="https://tupikitrust.org.nz">Tūpiki Trust</a>, is funding a total facelift for climbnz.org.nz.</p>
<p>When it launches in <strike>August</strike>October, the site will include improved search options including a route finder, sortable indexes and a map interface. It’ll work better on mobile, and have a completely new design.</p>
<p>This is just the first step – there’s a staged plan to introduce single-sign-on with alpineclub.org.nz, improved moderation, alerts and updates, logbooks, hitlists, downloadable guides and a topo editor.</p>
<p>Do you use the current API? Please <a href="webmaster[at]alpineclub.org.nz?subject=climbnz.org.nz">contact us</a> as the API won’t be updated initially when the site relaunches.</p>';
      return $block;

    case 'climbnz_intro':
      $block['subject'] = t('');

      $count = 0;
      $output = t('<p>The ClimbNZ database of New Zealand climbs, crags and mountains is for everyone who climbs in New Zealand. There are currently ~@count routes online.</p>', array('@count' => climbnz_count_route()));
      $output .= '<h4><a href="nz">Browse the database</a> (or use the search box above)</h4>';
      $output .= '<h4><a href="/user/register">Create an account</a> or <a href="/user">login</a> to edit.</h4>';
      $output .= '<p><strong>New:</strong> How to add routes to the site is explained on our <a href="faq">FAQ</a> page. <br>
Read <a href="/terms">Terms of Use</a> to understand how contributions are managed.</p>';
      $block['content'] = $output;
      return $block;

    case 'climbnz_intro_migration':
      $block['subject'] = t('Welcome to the updated climbnz.org.nz');
      $block['content'] = t('Welcome to the second iteration of climbnz.org.nz.
      <ul>
        <li>A new responsive theme based on Bootstrap means the site should be more usable on tablet and mobile.</li>
        <li>\'+\' subdivisions have been added to \'Alpine (Technical)\' grade options.</li>
        <li>An API for place and route data is in development (<a href="https://gitlab.com/kayakr/climbnz.org.nz/issues/53">#53</a>).</li>
        <li>Historic versions of Place and Routes edits have <em>not</em> been migrated.</li>
        <li>Conversions between geo-coordinates are not automatic (<a href="https://gitlab.com/kayakr/climbnz.org.nz/issues/52">#52</a>).</li>
      </ul>  
	<p>Most things should <em>just work</em> but if you notice an issue please <a href="mailto:webmaster[at]alpineclub.org.nz?subject=climbnz.org.nz">get in touch</a> or add an issue directly at Gitlab <a href="https://gitlab.com/kayakr/climbnz.org.nz/issues">https://gitlab.com/kayakr/climbnz.org.nz/issues</a>. Enjoy!</p>');
      return $block;

    case 'climbnz_footer_links':
      $block['subject'] = t('');
      $output = t(
        'This dataset, NZAC Route Database, is licensed under a <a href="https://creativecommons.org/licenses/by-nc-sa/3.0/nz/">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 New Zealand</a> licence.');
      $output .= '<br/>ClimbNZ is coordinated by the <a href="http://alpineclub.org.nz">New Zealand Alpine Club</a>.';
      $block['content'] = $output;
      return $block;
  }
}

function climbnz_count_route() {
  $count = db_query("SELECT COUNT(nid) FROM {node} WHERE type='route' AND status=1")->fetchField();
  return number_format($count);
}

function climbnz_place_route() {
  $count = db_query("SELECT COUNT(nid) FROM {node} WHERE type='place' AND status=1")->fetchField();
  return number_format($count);
}
