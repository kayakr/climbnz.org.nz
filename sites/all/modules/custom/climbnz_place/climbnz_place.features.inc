<?php

/**
 * @file
 * climbnz_place.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function climbnz_place_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function climbnz_place_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function climbnz_place_node_info() {
  $items = array(
    'place' => array(
      'name' => t('Place'),
      'base' => 'node_content',
      'description' => t('A place containing either other Places (e.g. region, crag, mountain, face, area, boulder, etc.) or a Route.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => t('Please confirm your hierarchy of places before adding new places. For advice on how to enter Place or Route data, consult our <a href="/faq">faq</a>.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
