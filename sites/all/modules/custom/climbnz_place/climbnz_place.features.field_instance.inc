<?php

/**
 * @file
 * climbnz_place.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function climbnz_place_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-place-field_access'.
  $field_instances['node-place-field_access'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Any access conditions or restrictions first. How to get to the place second. Best season etc. Data in this field is public.',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 9,
      ),
      'search_index' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_access',
    'label' => 'Access',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-place-field_access_private'.
  $field_instances['node-place-field_access_private'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Access notes that only registered users can see (e.g. landowner emails and phone numbers, etc.)',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 9,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 10,
      ),
      'search_index' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_access_private',
    'label' => 'Access (private)',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-place-field_altitude'.
  $field_instances['node-place-field_altitude'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Metres above sea level.',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'inline',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
      'search_index' => array(
        'label' => 'inline',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_altitude',
    'label' => 'Altitude',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => 'm',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-place-field_aspect'.
  $field_instances['node-place-field_aspect'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If you put your back against the rock, you are facing this direction. ',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'search_index' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_aspect',
    'label' => 'Aspect',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-place-field_attribution'.
  $field_instances['node-place-field_attribution'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If this material is being republished with permission, please attribute the original source.',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 14,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 15,
      ),
      'search_index' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_attribution',
    'label' => 'Attribution',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'node-place-field_description'.
  $field_instances['node-place-field_description'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'General introduction to the place. What kind of routes, what time of year, history, development, comparisons to other places, etc. Point out features of interest or particular hazards.',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'search_index' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-place-field_file_attachments'.
  $field_instances['node-place-field_file_attachments'] = array(
    'bundle' => 'place',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 13,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 14,
      ),
      'search_index' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_file_attachments',
    'label' => 'File attachments',
    'required' => 0,
    'settings' => array(
      'description_field' => 1,
      'file_directory' => 'attachments',
      'file_extensions' => 'txt pdf jpg odt',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'node-place-field_geo_lat_lon'.
  $field_instances['node-place-field_geo_lat_lon'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'In decimal degrees, e.g. -43.59556710 170.14219572',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'inline',
        'module' => 'geofield',
        'settings' => array(
          'data' => 'full',
          'format' => 'decimal_degrees',
          'labels' => 1,
        ),
        'type' => 'geofield_latlon',
        'weight' => 4,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'geofield',
        'settings' => array(
          'data' => 'full',
          'format' => 'decimal_degrees',
          'labels' => 0,
        ),
        'type' => 'geofield_latlon',
        'weight' => 5,
      ),
      'search_index' => array(
        'label' => 'inline',
        'module' => 'geofield',
        'settings' => array(
          'data' => 'full',
          'format' => 'decimal_degrees',
          'labels' => 0,
        ),
        'type' => 'geofield_latlon',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_geo_lat_lon',
    'label' => 'Lat/Lon',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'geofield',
      'settings' => array(
        'html5_geolocation' => 0,
      ),
      'type' => 'geofield_latlon',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-place-field_guide'.
  $field_instances['node-place-field_guide'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'What guidebooks does this place appear in?',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'links' => 0,
          'use_content_language' => 1,
          'view_mode' => 'teaser',
        ),
        'type' => 'entityreference_entity_view',
        'weight' => 15,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 20,
      ),
      'search_index' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 20,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_guide',
    'label' => 'This place appears in',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 16,
    ),
  );

  // Exported field_instance: 'node-place-field_image'.
  $field_instances['node-place-field_image'] = array(
    'bundle' => 'place',
    'deleted' => 0,
    'description' => 'Add topos or illustrative photos here.',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 10,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 11,
      ),
      'search_index' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'image',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-place-field_nzms260'.
  $field_instances['node-place-field_nzms260'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'e.g. H36 793307',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
      'search_index' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_nzms260',
    'label' => 'NZMS260',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 10,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-place-field_topo50'.
  $field_instances['node-place-field_topo50'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'e.g. BX15 693 691',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
      'search_index' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_topo50',
    'label' => 'Topo50',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 12,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-place-field_type'.
  $field_instances['node-place-field_type'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'What kind of place is this? Regions should match Regional Councils. Areas are zones containing walls or crags (e.g. Castle Hill). Protected Areas include National Parks.',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'search_index' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_type',
    'label' => 'Type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-place-field_walktime'.
  $field_instances['node-place-field_walktime'] = array(
    'bundle' => 'place',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Time to walk with from your car, to the first route included in the ‘place’. If a time from somewhere else makes more sense e.g. a hut, please define it in the Access field. Use min for minutes and hrs for hours.',
    'display' => array(
      'climbnz_field_guide' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
      'search_index' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_walktime',
    'label' => 'Walk time',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 10,
      ),
      'type' => 'text_textfield',
      'weight' => 9,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Access');
  t('Access (private)');
  t('Access notes that only registered users can see (e.g. landowner emails and phone numbers, etc.)');
  t('Add topos or illustrative photos here.');
  t('Altitude');
  t('Any access conditions or restrictions first. How to get to the place second. Best season etc. Data in this field is public.');
  t('Aspect');
  t('Attribution');
  t('Description');
  t('File attachments');
  t('General introduction to the place. What kind of routes, what time of year, history, development, comparisons to other places, etc. Point out features of interest or particular hazards.');
  t('If this material is being republished with permission, please attribute the original source.');
  t('If you put your back against the rock, you are facing this direction. ');
  t('Image');
  t('In decimal degrees, e.g. -43.59556710 170.14219572');
  t('Lat/Lon');
  t('Metres above sea level.');
  t('NZMS260');
  t('This place appears in');
  t('Time to walk with from your car, to the first route included in the ‘place’. If a time from somewhere else makes more sense e.g. a hut, please define it in the Access field. Use min for minutes and hrs for hours.');
  t('Topo50');
  t('Type');
  t('Walk time');
  t('What guidebooks does this place appear in?');
  t('What kind of place is this? Regions should match Regional Councils. Areas are zones containing walls or crags (e.g. Castle Hill). Protected Areas include National Parks.');
  t('e.g. BX15 693 691');
  t('e.g. H36 793307');

  return $field_instances;
}
