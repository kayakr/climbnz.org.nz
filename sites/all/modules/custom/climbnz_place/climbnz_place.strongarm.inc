<?php

/**
 * @file
 * climbnz_place.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function climbnz_place_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_place';
  $strongarm->value = 0;
  $export['comment_anonymous_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_place';
  $strongarm->value = 1;
  $export['comment_default_mode_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_place';
  $strongarm->value = '50';
  $export['comment_default_per_page_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_sorting_place';
  $strongarm->value = '2';
  $export['comment_default_sorting_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_place';
  $strongarm->value = 1;
  $export['comment_form_location_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_place';
  $strongarm->value = '2';
  $export['comment_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_place';
  $strongarm->value = '1';
  $export['comment_preview_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_place';
  $strongarm->value = 0;
  $export['comment_subject_field_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_place';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_place';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_place';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__place';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => TRUE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'pitches_on_route' => array(
        'custom_settings' => FALSE,
      ),
      'climbnz_field_guide' => array(
        'custom_settings' => FALSE,
      ),
      'ief_table' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '15',
        ),
        'redirect' => array(
          'weight' => '14',
        ),
      ),
      'display' => array(
        'nodehierarchy_children' => array(
          'default' => array(
            'weight' => '19',
            'visible' => FALSE,
          ),
          'climbnz_field_guide' => array(
            'weight' => '11',
            'visible' => TRUE,
          ),
          'search_index' => array(
            'weight' => '18',
            'visible' => TRUE,
          ),
        ),
        'nodehierarchy_children_links' => array(
          'default' => array(
            'weight' => '12',
            'visible' => TRUE,
          ),
          'climbnz_field_guide' => array(
            'weight' => '12',
            'visible' => TRUE,
          ),
          'search_index' => array(
            'weight' => '11',
            'visible' => TRUE,
          ),
        ),
        'route_table' => array(
          'default' => array(
            'weight' => '13',
            'visible' => TRUE,
          ),
          'search_index' => array(
            'weight' => '12',
            'visible' => TRUE,
          ),
        ),
        'place_table' => array(
          'default' => array(
            'weight' => '16',
            'visible' => TRUE,
          ),
          'search_index' => array(
            'weight' => '15',
            'visible' => TRUE,
          ),
        ),
        'guide_list' => array(
          'default' => array(
            'weight' => '17',
            'visible' => TRUE,
          ),
          'search_index' => array(
            'weight' => '16',
            'visible' => TRUE,
          ),
        ),
        'place_minimap' => array(
          'default' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
          'search_index' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
        ),
        'flag_email_node' => array(
          'default' => array(
            'weight' => '18',
            'visible' => TRUE,
          ),
          'search_index' => array(
            'weight' => '17',
            'visible' => TRUE,
          ),
        ),
        'climbnz_uuid' => array(
          'default' => array(
            'weight' => '20',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_place';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_place';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_place';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_place';
  $strongarm->value = '1';
  $export['node_preview_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_place';
  $strongarm->value = 0;
  $export['node_submitted_place'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_place_pattern';
  $strongarm->value = '[node:nodehierarchy:parent:url:path]/[node:title]';
  $export['pathauto_node_place_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_place';
  $strongarm->value = array(
    'status' => '1',
    'priority' => '0.8',
  );
  $export['xmlsitemap_settings_node_place'] = $strongarm;

  return $export;
}
