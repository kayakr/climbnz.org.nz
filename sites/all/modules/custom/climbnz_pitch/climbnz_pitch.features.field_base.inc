<?php
/**
 * @file
 * climbnz_pitch.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function climbnz_pitch_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_grade_aid'.
  $field_bases['field_grade_aid'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_aid',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        6 => 'A5',
        5 => 'A4',
        4 => 'A3',
        3 => 'A2',
        2 => 'A1',
        1 => 'A0',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_alpine_commitment'.
  $field_bases['field_grade_alpine_commitment'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_alpine_commitment',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        6 => 'VI',
        5 => 'V',
        4 => 'IV',
        3 => 'III',
        2 => 'II',
        1 => 'I',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_alpine_mt_cook'.
  $field_bases['field_grade_alpine_mt_cook'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_alpine_mt_cook',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        '7.25' => '7+',
        7 => 7,
        '6.75' => '7-',
        '6.25' => '6+',
        6 => 6,
        '5.75' => '6-',
        '5.25' => '5+',
        5 => 5,
        '4.75' => '5-',
        '4.25' => '4+',
        4 => 4,
        '3.75' => '4-',
        '3.25' => '3+',
        3 => 3,
        '2.75' => '3-',
        '2.25' => '2+',
        2 => 2,
        '1.75' => '2-',
        '1.25' => '1+',
        1 => 1,
        '.75' => '1-',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_alpine_technical'.
  $field_bases['field_grade_alpine_technical'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_alpine_technical',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        7 => 7,
        6 => 6,
        5 => 5,
        4 => 4,
        3 => 3,
        2 => 2,
        1 => 1,
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_boulder'.
  $field_bases['field_grade_boulder'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_boulder',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        17 => 'V17',
        16 => 'V16',
        15 => 'V15',
        14 => 'V14',
        13 => 'V13',
        12 => 'V12',
        11 => 'V11',
        10 => 'V10',
        9 => 'V9',
        8 => 'V8',
        7 => 'V7',
        6 => 'V6',
        5 => 'V5',
        4 => 'V4',
        3 => 'V3',
        2 => 'V2',
        1 => 'V1',
        0 => 'V0+',
        -1 => 'V0',
        -2 => 'VM',
        -3 => 'VE',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_ewbank'.
  $field_bases['field_grade_ewbank'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_ewbank',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        40 => 40,
        39 => 39,
        38 => 38,
        37 => 37,
        36 => 36,
        35 => 35,
        34 => 34,
        33 => 33,
        32 => 32,
        31 => 31,
        30 => 30,
        29 => 29,
        28 => 28,
        27 => 27,
        26 => 26,
        25 => 25,
        24 => 24,
        23 => 23,
        22 => 22,
        21 => 21,
        20 => 20,
        19 => 19,
        18 => 18,
        17 => 17,
        16 => 16,
        15 => 15,
        14 => 14,
        13 => 13,
        12 => 12,
        11 => 11,
        10 => 10,
        9 => 9,
        8 => 8,
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_mixed'.
  $field_bases['field_grade_mixed'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_mixed',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        12 => 'M12',
        11 => 'M11',
        10 => 'M10',
        9 => 'M9',
        8 => 'M8',
        7 => 'M7',
        6 => 'M6',
        5 => 'M5',
        4 => 'M4',
        3 => 'M3',
        2 => 'M2',
        1 => 'M1',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_grade_water_ice'.
  $field_bases['field_grade_water_ice'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_grade_water_ice',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        7 => 'WI7',
        6 => 'WI6',
        5 => 'WI5',
        4 => 'WI4',
        3 => 'WI3',
        2 => 'WI2',
        1 => 'WI1',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
