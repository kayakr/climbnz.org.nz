<?php
/**
 * @file
 * climbnz_pitch.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function climbnz_pitch_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function climbnz_pitch_node_info() {
  $items = array(
    'pitch' => array(
      'name' => t('Pitch'),
      'base' => 'node_content',
      'description' => t('Section of a climb with specific description and grade.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
