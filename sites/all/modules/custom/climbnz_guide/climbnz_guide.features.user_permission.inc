<?php

/**
 * @file
 * climbnz_guide.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function climbnz_guide_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create guide content'.
  $permissions['create guide content'] = array(
    'name' => 'create guide content',
    'roles' => array(
      'Editor' => 'Editor',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any guide content'.
  $permissions['delete any guide content'] = array(
    'name' => 'delete any guide content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own guide content'.
  $permissions['delete own guide content'] = array(
    'name' => 'delete own guide content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any guide content'.
  $permissions['edit any guide content'] = array(
    'name' => 'edit any guide content',
    'roles' => array(
      'Editor' => 'Editor',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own guide content'.
  $permissions['edit own guide content'] = array(
    'name' => 'edit own guide content',
    'roles' => array(
      'Editor' => 'Editor',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
