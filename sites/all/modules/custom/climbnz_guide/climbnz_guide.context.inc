<?php

/**
 * @file
 * climbnz_guide.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function climbnz_guide_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'guide_blocks';
  $context->description = 'Show block listing Places on Guide page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'guide' => 'guide',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-climbnz_places_in_guide-block' => array(
          'module' => 'views',
          'delta' => 'climbnz_places_in_guide-block',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Show block listing Places on Guide page');
  $export['guide_blocks'] = $context;

  return $export;
}
