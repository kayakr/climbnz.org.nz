<?php
/**
 * @file
 * climbnz_main_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function climbnz_main_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_crags:crags.
  $menu_links['main-menu_crags:crags'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'crags',
    'router_path' => 'crags',
    'link_title' => 'Crags',
    'options' => array(
      'identifier' => 'main-menu_crags:crags',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_guides:guide.
  $menu_links['main-menu_guides:guide'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'guide',
    'router_path' => 'guide',
    'link_title' => 'Guides',
    'options' => array(
      'identifier' => 'main-menu_guides:guide',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_mountains:mountains.
  $menu_links['main-menu_mountains:mountains'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'mountains',
    'router_path' => 'mountains',
    'link_title' => 'Mountains',
    'options' => array(
      'identifier' => 'main-menu_mountains:mountains',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Crags');
  t('Guides');
  t('Home');
  t('Mountains');

  return $menu_links;
}
