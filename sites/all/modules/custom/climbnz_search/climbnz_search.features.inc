<?php
/**
 * @file
 * climbnz_search.features.inc
 */

/**
 * Implements hook_views_api().
 */
function climbnz_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
