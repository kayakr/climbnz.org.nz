<?php

/**
 * @file
 * climbnz_homepage.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function climbnz_homepage_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'climbnz_homepage';
  $context->description = 'Blocks for ClimbNZ homepage';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'climbnz_homepage-climbnz_hero_image' => array(
          'module' => 'climbnz_homepage',
          'delta' => 'climbnz_hero_image',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'climbnz-climbnz_intro' => array(
          'module' => 'climbnz',
          'delta' => 'climbnz_intro',
          'region' => 'content_top',
          'weight' => '-9',
        ),
        'views-climbnz_activity-block_1' => array(
          'module' => 'views',
          'delta' => 'climbnz_activity-block_1',
          'region' => 'content',
          'weight' => '-1',
        ),
        'comment-recent' => array(
          'module' => 'comment',
          'delta' => 'recent',
          'region' => 'content',
          'weight' => '-1',
        ),
        'user-online' => array(
          'module' => 'user',
          'delta' => 'online',
          'region' => 'content',
          'weight' => '-1',
        ),
        'user-new' => array(
          'module' => 'user',
          'delta' => 'new',
          'region' => 'content',
          'weight' => '-1',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks for ClimbNZ homepage');
  $export['climbnz_homepage'] = $context;

  return $export;
}
