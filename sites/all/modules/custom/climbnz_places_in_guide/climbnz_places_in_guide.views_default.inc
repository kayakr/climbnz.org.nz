<?php

/**
 * @file
 * climbnz_places_in_guide.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function climbnz_places_in_guide_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'climbnz_places_in_guide';
  $view->description = 'List of Places that reference a Guide.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Places in Guide';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Places in Guide';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_guide_target_id']['id'] = 'field_guide_target_id';
  $handler->display->display_options['relationships']['field_guide_target_id']['table'] = 'field_data_field_guide';
  $handler->display->display_options['relationships']['field_guide_target_id']['field'] = 'field_guide_target_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: This place appears in (field_guide) */
  $handler->display->display_options['arguments']['field_guide_target_id']['id'] = 'field_guide_target_id';
  $handler->display->display_options['arguments']['field_guide_target_id']['table'] = 'field_data_field_guide';
  $handler->display->display_options['arguments']['field_guide_target_id']['field'] = 'field_guide_target_id';
  $handler->display->display_options['arguments']['field_guide_target_id']['relationship'] = 'reverse_field_guide_node';
  $handler->display->display_options['arguments']['field_guide_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_guide_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_guide_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_guide_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_guide_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'place' => 'place',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['climbnz_places_in_guide'] = $view;

  return $export;
}
