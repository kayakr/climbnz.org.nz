<?php

/**
 * @file
 * climbnz_places_in_guide.features.inc
 */

/**
 * Implements hook_views_api().
 */
function climbnz_places_in_guide_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
