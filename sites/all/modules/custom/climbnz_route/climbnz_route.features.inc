<?php

/**
 * @file
 * climbnz_route.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function climbnz_route_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function climbnz_route_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function climbnz_route_image_default_styles() {
  $styles = array();

  // Exported image style: climbnz-md.
  $styles['climbnz-md'] = array(
    'label' => 'climbnz-md',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 700,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function climbnz_route_node_info() {
  $items = array(
    'route' => array(
      'name' => t('Route'),
      'base' => 'node_content',
      'description' => t('An alpine route, rock climb or boulder problem.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
