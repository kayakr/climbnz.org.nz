<?php

/**
 * @file
 * climbnz_route.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function climbnz_route_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'climbnz_route_table';
  $view->description = 'Table of Routes for a Place.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'ClimbNZ Route Table';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'ClimbNZ Route Table';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Reference */
  $handler->display->display_options['fields']['field_reference']['id'] = 'field_reference';
  $handler->display->display_options['fields']['field_reference']['table'] = 'field_data_field_reference';
  $handler->display->display_options['fields']['field_reference']['field'] = 'field_reference';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Grade */
  $handler->display->display_options['fields']['field_grade']['id'] = 'field_grade';
  $handler->display->display_options['fields']['field_grade']['table'] = 'field_data_field_grade';
  $handler->display->display_options['fields']['field_grade']['field'] = 'field_grade';
  /* Field: Content: Length */
  $handler->display->display_options['fields']['field_length']['id'] = 'field_length';
  $handler->display->display_options['fields']['field_length']['table'] = 'field_data_field_length';
  $handler->display->display_options['fields']['field_length']['field'] = 'field_length';
  $handler->display->display_options['fields']['field_length']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_length']['alter']['text'] = '[field_length]m';
  $handler->display->display_options['fields']['field_length']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_length']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_length']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Quality */
  $handler->display->display_options['fields']['field_quality']['id'] = 'field_quality';
  $handler->display->display_options['fields']['field_quality']['table'] = 'field_data_field_quality';
  $handler->display->display_options['fields']['field_quality']['field'] = 'field_quality';
  $handler->display->display_options['fields']['field_quality']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_quality']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_quality']['click_sort_column'] = 'rating';
  $handler->display->display_options['fields']['field_quality']['settings'] = array(
    'widget' => array(
      'fivestar_widget' => 'default',
    ),
    'expose' => 0,
    'style' => 'average',
    'text' => 'none',
  );
  /* Field: Content: Bolts */
  $handler->display->display_options['fields']['field_bolts']['id'] = 'field_bolts';
  $handler->display->display_options['fields']['field_bolts']['table'] = 'field_data_field_bolts';
  $handler->display->display_options['fields']['field_bolts']['field'] = 'field_bolts';
  $handler->display->display_options['fields']['field_bolts']['empty_zero'] = TRUE;
  /* Field: Content: Gone */
  $handler->display->display_options['fields']['field_gone']['id'] = 'field_gone';
  $handler->display->display_options['fields']['field_gone']['table'] = 'field_data_field_gone';
  $handler->display->display_options['fields']['field_gone']['field'] = 'field_gone';
  $handler->display->display_options['fields']['field_gone']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_gone']['empty_zero'] = TRUE;
  /* Field: Content: Natural pro */
  $handler->display->display_options['fields']['field_natural_pro']['id'] = 'field_natural_pro';
  $handler->display->display_options['fields']['field_natural_pro']['table'] = 'field_data_field_natural_pro';
  $handler->display->display_options['fields']['field_natural_pro']['field'] = 'field_natural_pro';
  $handler->display->display_options['fields']['field_natural_pro']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_natural_pro']['alter']['text'] = '<img src="/sites/all/themes/climbnztheme/images/wire2.gif" alt="wire representing trad"/>';
  $handler->display->display_options['fields']['field_natural_pro']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_natural_pro']['empty_zero'] = TRUE;
  /* Field: Content: Route Image */
  $handler->display->display_options['fields']['field_route_image']['id'] = 'field_route_image';
  $handler->display->display_options['fields']['field_route_image']['table'] = 'field_data_field_route_image';
  $handler->display->display_options['fields']['field_route_image']['field'] = 'field_route_image';
  $handler->display->display_options['fields']['field_route_image']['label'] = '';
  $handler->display->display_options['fields']['field_route_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_route_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_route_image']['type'] = 'climbnz_route_image_formatter';
  $handler->display->display_options['fields']['field_route_image']['delta_offset'] = '0';
  /* Field: Content: Comment count */
  $handler->display->display_options['fields']['comment_count']['id'] = 'comment_count';
  $handler->display->display_options['fields']['comment_count']['table'] = 'node_comment_statistics';
  $handler->display->display_options['fields']['comment_count']['field'] = 'comment_count';
  $handler->display->display_options['fields']['comment_count']['label'] = '';
  $handler->display->display_options['fields']['comment_count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['comment_count']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['comment_count']['empty_zero'] = TRUE;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'field_data_field_description';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['exclude'] = TRUE;
  /* Field: Content: Pitch(es) */
  $handler->display->display_options['fields']['field_pitch']['id'] = 'field_pitch';
  $handler->display->display_options['fields']['field_pitch']['table'] = 'field_data_field_pitch';
  $handler->display->display_options['fields']['field_pitch']['field'] = 'field_pitch';
  $handler->display->display_options['fields']['field_pitch']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_pitch']['click_sort_column'] = 'grade_ewbank';
  $handler->display->display_options['fields']['field_pitch']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_pitch']['multi_type'] = 'ol';
  /* Field: Content: Ascent */
  $handler->display->display_options['fields']['field_ascent']['id'] = 'field_ascent';
  $handler->display->display_options['fields']['field_ascent']['table'] = 'field_data_field_ascent';
  $handler->display->display_options['fields']['field_ascent']['field'] = 'field_ascent';
  $handler->display->display_options['fields']['field_ascent']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_ascent']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_ascent']['multi_type'] = 'ul';
  /* Sort criterion: Node Hierarchy: Child Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'nh_menu_links';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  /* Contextual filter: Node Hierarchy: Parent Node ID */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'nh_parent';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'route' => 'route',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'climbnz-route-table';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['climbnz_route_table'] = $view;

  return $export;
}
