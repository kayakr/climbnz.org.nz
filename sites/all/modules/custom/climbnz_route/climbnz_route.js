(function ($) {

  // Reduce visual impact of placeholder stars.
  Drupal.behaviors.climbnzRouteType = {
    attach: function (context, settings) {
      $('.field-name-field-route-type input').change(function(e) {
        updatePitch();
      });

      updatePitch(); // init.
    }
  };

  function updatePitch() {
    var type = $('.field-name-field-route-type input:radio:checked').val();
    console.log('type=' + type);

    switch (type) {
      case 'alpine':
        $('select.grade-ewbank').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-aid').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-alpine-technical').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-alpine-mtcook').removeAttr('disabled').siblings('label').css({'color': '#000'});
        //$('.pitch-grade-alpine-technical').css('display', 'table-cell');
        //$('.grade_alpine_technical').css('display', 'table-cell');

        $('select.grade-alpine-commitment').removeAttr('disabled').siblings('label').css({'color': '#000'});
        //$('.pitch-grade-alpine-commitment').css('display', 'table-cell');
        //$('.grade_alpine_commitment').css('display', 'table-cell');

        $('select.grade-water-ice').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-mixed').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-boulder').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        break;

      case 'rock':
        $('select.grade-ewbank').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-aid').removeAttr('disabled').siblings('label').css({'color': '#000'});

        $('select.grade-alpine-technical').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-alpine-mtcook').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        //$('.pitch-grade-alpine-technical').css('display', 'none');
        //$('.grade_alpine_technical').css('display', 'none');

        $('select.grade-alpine-commitment').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        //$('.pitch-grade-alpine-commitment').css('display', 'none');
        //$('.grade_alpine_commitment').css('display', 'none');

        $('select.grade-water-ice').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-mixed').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-boulder').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('input.grade-bolts').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('input.grade-trad:checkbox').removeAttr('disabled').siblings('label').css({'color': '#000'});
        break;

      case 'ice':
        $('select.grade-ewbank').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-aid').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});

        $('select.grade-alpine-technical').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-alpine-mtcook').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});

        $('select.grade-alpine-commitment').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});

        $('select.grade-water-ice').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-mixed').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-boulder').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('input.grade-trad:checkbox').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        break;

      case 'boulder':
        $('select.grade-ewbank').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-aid').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});

        $('select.grade-alpine-technical').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-alpine-mtcook').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});

        $('select.grade-alpine-commitment').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});

        $('select.grade-water-ice').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-mixed').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-boulder').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('input.grade-bolts').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('input.grade-trad:checkbox').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        break;

      case 'ski':
        $('select.grade-alpine-technical').removeAttr('disabled').siblings('label').css({'color': '#000'});
        $('select.grade-alpine-mtcook').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-alpine-commitment').removeAttr('disabled').siblings('label').css({'color': '#000'});

        $('select.grade-ewbank').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-aid').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});

        $('select.grade-water-ice').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-mixed').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('select.grade-boulder').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('input.grade-bolts').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        $('input.grade-trad:checkbox').attr('disabled', 'disabled').siblings('label').css({'color': 'graytext'});
        break;
    } // /switch.
  };

})(jQuery);
