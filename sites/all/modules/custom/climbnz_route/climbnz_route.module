<?php
/**
 * @file
 * Code for the ClimbNZ Route feature.
 */

include_once 'climbnz_route.features.inc';

/**
 * Implements hook_form_alter().
 */
function climbnz_route_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'route_node_form') {
    // Add some custom CSS to the form to show Route type options in row.
    drupal_add_css(drupal_get_path('module', 'climbnz_route') . '/climbnz_route.css');
    drupal_add_js(drupal_get_path('module', 'climbnz_route') . '/climbnz_route.js');

    $form['title']['#description'] = t('Please note NZAC’s position that we will not publish names that are discriminatory, derogatory or likely to cause harm.');
  }
}

/**
 * Save aggregated Pitch grade and length data to Route fields.
 *
 * Implements hook_node_presave().
 * @param $node
 */
function climbnz_route_node_presave($node) {
  if ($node->type == 'route') {
    // Avoid PHP warning if no pitch data entered.
    if (isset($node->field_pitch[LANGUAGE_NONE][0])) {
      // If first Pitch is not empty, or we have multiple Pitches.
      if (!climbnz_pitch_field_is_empty($node->field_pitch[LANGUAGE_NONE][0], array()) ||
        count($node->field_pitch[LANGUAGE_NONE]) > 1) {
        $grades = array();
        $length = 0;
        $bolts = 0;
        $natural_pro = 0;

        $grade_ewbank_options = climbnz_pitch_grade_ewbank_options();
        $grade_alpine_technical_options = climbnz_pitch_grade_alpine_technical_options();
        $grade_alpine_commitment_options = climbnz_pitch_grade_alpine_commitment_options();
        $grade_alpine_mtcook_options = climbnz_pitch_grade_alpine_mtcook_options();
        $grade_aid_options = climbnz_pitch_grade_aid_options();
        $grade_water_ice_options = climbnz_pitch_grade_water_ice_options();
        $grade_mixed_options = climbnz_pitch_grade_mixed_options();
        $grade_boulder_options = climbnz_pitch_grade_boulder_options();

        foreach ($node->field_pitch[LANGUAGE_NONE] as $pitch) {
          // Show aggregate value.
          switch ($node->field_route_type[LANGUAGE_NONE][0]['value']) {
            case 'alpine':
              $route_grades = array(
                !empty($pitch['grade_alpine_commitment']) ? $grade_alpine_commitment_options[$pitch['grade_alpine_commitment']] : NULL,
                !empty($pitch['grade_alpine_technical']) ? $grade_alpine_technical_options[$pitch['grade_alpine_technical']] : NULL,
                !empty($pitch['grade_alpine_mtcook']) ? $grade_alpine_mtcook_options[$pitch['grade_alpine_mtcook']] : NULL,
                !empty($pitch['grade_ewbank']) ? $grade_ewbank_options[$pitch['grade_ewbank']] : NULL,
                !empty($pitch['grade_aid']) ? $grade_aid_options[$pitch['grade_aid']] : NULL,
                !empty($pitch['grade_water_ice']) ? $grade_water_ice_options[$pitch['grade_water_ice']] : NULL,
                !empty($pitch['grade_mixed']) ? $grade_mixed_options[$pitch['grade_mixed']] : NULL,
              );
              $grades[] = trim(implode(' ', $route_grades));
              break;

            // Simply propagate Boulder grade.
            case 'boulder':
              $grades[] = !empty($pitch['grade_boulder']) ? $grade_boulder_options[$pitch['grade_boulder']] : NULL;
              break;

            case 'ice':
              $route_grades = array(
                !empty($pitch['grade_water_ice']) ? $grade_water_ice_options[$pitch['grade_water_ice']] : NULL,
                !empty($pitch['grade_mixed']) ? $grade_mixed_options[$pitch['grade_mixed']] : NULL,
              );
              $grades[] = trim(implode(' ', $route_grades));
              break;

            // Show ewbank and aid, unless either is zero.
            case 'rock':
              $route_grades = array(
                $pitch['grade_ewbank'] != 0 ? $grade_ewbank_options[$pitch['grade_ewbank']] : NULL,
                !empty($pitch['grade_aid']) ? $grade_aid_options[$pitch['grade_aid']] : NULL,
              );
              $grades[] = trim(implode(' ', $route_grades));
              break;
          }

          $length += $pitch['length'];
          $bolts += $pitch['bolts'];
          $natural_pro += $pitch['trad'];
        }

        $node->field_grade[LANGUAGE_NONE][0]['value'] = implode(',', $grades);
        $node->field_length[LANGUAGE_NONE][0]['value'] = $length;
        $node->field_bolts[LANGUAGE_NONE][0]['value'] = $bolts;
        $node->field_natural_pro[LANGUAGE_NONE][0]['value'] = $natural_pro ? 1 : 0;
      }
    }
  }
}

/**
 * Return grade for a pitch based on route type, e.g. alpine, rock.
 *
 * @param $route_type
 * @param $pitch
 *
 * @return array|string
 */
function climbnz_route_pitch_grades($route_type, $pitch) {
  $grades = array();
  $grade_ewbank_options = climbnz_pitch_grade_ewbank_options();
  $grade_alpine_technical_options = climbnz_pitch_grade_alpine_technical_options();
  $grade_alpine_commitment_options = climbnz_pitch_grade_alpine_commitment_options();
  $grade_alpine_mtcook_options = climbnz_pitch_grade_alpine_mtcook_options();
  $grade_aid_options = climbnz_pitch_grade_aid_options();
  $grade_water_ice_options = climbnz_pitch_grade_water_ice_options();
  $grade_mixed_options = climbnz_pitch_grade_mixed_options();
  $grade_boulder_options = climbnz_pitch_grade_boulder_options();

  switch ($route_type) {
    case 'alpine':
      $route_grades = array(
        !empty($pitch['grade_alpine_commitment']) ? $grade_alpine_commitment_options[$pitch['grade_alpine_commitment']] : NULL,
        !empty($pitch['grade_alpine_technical']) ? $grade_alpine_technical_options[$pitch['grade_alpine_technical']] : NULL,
        !empty($pitch['grade_alpine_mtcook']) ? $grade_alpine_mtcook_options[$pitch['grade_alpine_mtcook']] : NULL,
        !empty($pitch['grade_ewbank']) ? $grade_ewbank_options[$pitch['grade_ewbank']] : NULL,
        !empty($pitch['grade_aid']) ? $grade_aid_options[$pitch['grade_aid']] : NULL,
        !empty($pitch['grade_water_ice']) ? $grade_water_ice_options[$pitch['grade_water_ice']] : NULL,
        !empty($pitch['grade_mixed']) ? $grade_mixed_options[$pitch['grade_mixed']] : NULL,
      );
      break;

  }
  $grades = trim(implode(' ', $route_grades));

  return $grades;
}

/**
 * Register custom formatter for Route image field.
 * This can be used to indicate whether a Route has 1..N images.
 *
 * Implements hook_field_formatter_info().
 */
function climbnz_route_field_formatter_info() {
  return array(
    'climbnz_route_image_formatter' => array( //Machine name of the formatter
      'label' => t('ClimbNZ: Images are present icon'),
      'field types' => array('file'), //This will only be available to files
      //'settings'  => array( //Array of the settings we'll create
      //  'pic_size' => 'small', //give a default value for when the form is first loaded
      //  'tooltip' => 'Link to user Facebook page', //ditto
      //),
    ),
  );
}

/**
 * Somehow this gets invoked when Portage formatter above is specified in views,
 * for field_portage.
 *
 * Implements hook_field_formatter_view().
 */
function climbnz_route_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  $output = '';

  switch ($display['type']) {

    case 'climbnz_route_image_formatter':
      if (count($items)) {
        $element[0]['#markup'] = '<span class="glyphicon glyphicon-picture" title="One or more Route images exist." aria-hidden="true"></span>';
        if (count($items) > 1) {
          $element[0]['#markup'] .= '<sup>' . count($items) . '</sup>';
        }
      }
      break;
  }

  return $element;
}

/**
 * Declare UUID as extra field so it can be displayed on Route page.
 *
 * Implements hook_field_extra_fields().
 * @return mixed
 */
function climbnz_route_field_extra_fields() {
  $extra['node']['route'] = array(
    'display' => array(
      'climbnz_uuid' => array(
        'label' => t('UUID'),
        'description' => t('Node uuid (used in API)'),
        'weight' => 20,
      ),
    ),
  );
  return $extra;
}

/**
 * Inject UUID.
 *
 * Implements hook_node_view().
 */
function climbnz_route_node_view($node, $view_mode, $langcode) {
  global $user;

  if ($node->type == 'route' && $view_mode == 'full') {
    $extra_fields = field_extra_fields_get_display('node', $node->type, $view_mode);
    $node->content['climbnz_uuid'] = array(
      '#weight' => $extra_fields['climbnz_uuid']['weight'],
      '#markup' => '<div class="field field-name-uuid field-type-text field-label-inline">
<div class="field-label">UUID:&nbsp;</div>
<div class="field-items">
  <div class="field-item">' . $node->uuid . '</div>
</div>
</div>',
    );
  }
}
