<?php

/**
 * @file
 * climbnz_route.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function climbnz_route_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create route content'.
  $permissions['create route content'] = array(
    'name' => 'create route content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any route content'.
  $permissions['delete any route content'] = array(
    'name' => 'delete any route content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own route content'.
  $permissions['delete own route content'] = array(
    'name' => 'delete own route content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any route content'.
  $permissions['edit any route content'] = array(
    'name' => 'edit any route content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own route content'.
  $permissions['edit own route content'] = array(
    'name' => 'edit own route content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
