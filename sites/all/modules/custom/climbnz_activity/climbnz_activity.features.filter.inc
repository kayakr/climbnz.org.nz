<?php

/**
 * @file
 * climbnz_activity.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function climbnz_activity_filter_default_formats() {
  $formats = array();

  // Exported format: Full HTML no breaks.
  $formats['climbnz_full_html_no_breaks'] = array(
    'format' => 'climbnz_full_html_no_breaks',
    'name' => 'Full HTML no breaks',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
