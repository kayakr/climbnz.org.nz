<?php

/**
 * @file
 * climbnz_activity.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function climbnz_activity_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer message types'.
  $permissions['administer message types'] = array(
    'name' => 'administer message types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message',
  );

  // Exported permission: 'create messages'.
  $permissions['create messages'] = array(
    'name' => 'create messages',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message',
  );

  return $permissions;
}
