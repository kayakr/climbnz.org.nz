<?php

/**
 * @file
 * climbnz_activity.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function climbnz_activity_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function climbnz_activity_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_message_type().
 */
function climbnz_activity_default_message_type() {
  $items = array();
  $items['climbnz_place_comment_add'] = entity_import('message_type', '{
    "name" : "climbnz_place_comment_add",
    "description" : "ClimbNZ Place comment add",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-comment-ref:author:url]\\u0022\\u003E[message:field-comment-ref:author]\\u003C\\/a\\u003E commented \\u003Cq cite=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E\\u003Ca href=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E[message:field-comment-ref:subject]\\u003C\\/a\\u003E\\u003C\\/q\\u003E on \\u003Cspan class=\\u0022place-type\\u0022\\u003E[message:field-node-ref:field_type]\\u003C\\/span\\u003E \\u003Ca href=\\u0022[message:field-comment-ref:node:url]\\u0022\\u003E[message:field-comment-ref:node:title]\\u003C\\/a\\u003E.",
          "format" : "climbnz_full_html_no_breaks",
          "safe_value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-comment-ref:author:url]\\u0022\\u003E[message:field-comment-ref:author]\\u003C\\/a\\u003E commented \\u003Cq cite=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E\\u003Ca href=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E[message:field-comment-ref:subject]\\u003C\\/a\\u003E\\u003C\\/q\\u003E on \\u003Cspan class=\\u0022place-type\\u0022\\u003E[message:field-node-ref:field_type]\\u003C\\/span\\u003E \\u003Ca href=\\u0022[message:field-comment-ref:node:url]\\u0022\\u003E[message:field-comment-ref:node:title]\\u003C\\/a\\u003E."
        }
      ]
    },
    "rdf_mapping" : []
  }');
  $items['climbnz_place_node_add'] = entity_import('message_type', '{
    "name" : "climbnz_place_node_add",
    "description" : "ClimbNZ Place node add",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-node-ref:author:url]\\u0022\\u003E[message:field-node-ref:author:name]\\u003C\\/a\\u003E added \\u003Cspan class=\\u0022place-type\\u0022\\u003E[message:field-node-ref:field_type]\\u003C\\/span\\u003E \\u003Ca href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E.",
          "format" : "climbnz_full_html_no_breaks",
          "safe_value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-node-ref:author:url]\\u0022\\u003E[message:field-node-ref:author:name]\\u003C\\/a\\u003E added \\u003Cspan class=\\u0022place-type\\u0022\\u003E[message:field-node-ref:field_type]\\u003C\\/span\\u003E \\u003Ca href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E."
        }
      ]
    },
    "rdf_mapping" : []
  }');
  $items['climbnz_place_node_update'] = entity_import('message_type', '{
    "name" : "climbnz_place_node_update",
    "description" : "ClimbNZ Place node update",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:user:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:user:url:relative]\\u0022\\u003E[message:user:name]\\u003C\\/a\\u003E updated \\u003Cspan class=\\u0022place-type\\u0022\\u003E[message:field-node-ref:field_type]\\u003C\\/span\\u003E \\u003Ca href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E.",
          "format" : "climbnz_full_html_no_breaks",
          "safe_value" : "[message:user:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:user:url:relative]\\u0022\\u003E[message:user:name]\\u003C\\/a\\u003E updated \\u003Cspan class=\\u0022place-type\\u0022\\u003E[message:field-node-ref:field_type]\\u003C\\/span\\u003E \\u003Ca href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E."
        }
      ]
    },
    "rdf_mapping" : []
  }');
  $items['climbnz_route_comment_add'] = entity_import('message_type', '{
    "name" : "climbnz_route_comment_add",
    "description" : "ClimbNZ Route comment add",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-comment-ref:author:url]\\u0022\\u003E[message:field-comment-ref:author]\\u003C\\/a\\u003E commented \\u003Cq cite=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E\\u003Ca href=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E[message:field-comment-ref:subject]\\u003C\\/a\\u003E\\u003C\\/q\\u003E on route \\u003Ca href=\\u0022[message:field-comment-ref:node:url]\\u0022\\u003E[message:field-comment-ref:node:title]\\u003C\\/a\\u003E.",
          "format" : "climbnz_full_html_no_breaks",
          "safe_value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-comment-ref:author:url]\\u0022\\u003E[message:field-comment-ref:author]\\u003C\\/a\\u003E commented \\u003Cq cite=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E\\u003Ca href=\\u0022[message:field-comment-ref:url:relative]\\u0022\\u003E[message:field-comment-ref:subject]\\u003C\\/a\\u003E\\u003C\\/q\\u003E on route \\u003Ca href=\\u0022[message:field-comment-ref:node:url]\\u0022\\u003E[message:field-comment-ref:node:title]\\u003C\\/a\\u003E."
        }
      ]
    },
    "rdf_mapping" : []
  }');
  $items['climbnz_route_node_add'] = entity_import('message_type', '{
    "name" : "climbnz_route_node_add",
    "description" : "ClimbNZ Route node add",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-node-ref:author:url]\\u0022\\u003E[message:field-node-ref:author:name]\\u003C\\/a\\u003E added \\u003Cspan class=\\u0022route-type\\u0022\\u003E[message:field-node-ref:field_route_type]\\u003C\\/span\\u003E route \\u003Ca class=\\u0022route-title\\u0022 href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E.",
          "format" : "climbnz_full_html_no_breaks",
          "safe_value" : "[message:field-node-ref:author:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:field-node-ref:author:url]\\u0022\\u003E[message:field-node-ref:author:name]\\u003C\\/a\\u003E added \\u003Cspan class=\\u0022route-type\\u0022\\u003E[message:field-node-ref:field_route_type]\\u003C\\/span\\u003E route \\u003Ca class=\\u0022route-title\\u0022 href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E."
        }
      ]
    },
    "rdf_mapping" : []
  }');
  $items['climbnz_route_node_update'] = entity_import('message_type', '{
    "name" : "climbnz_route_node_update",
    "description" : "ClimbNZ Route node update",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:user:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:user:url:relative]\\u0022\\u003E[message:user:name]\\u003C\\/a\\u003E updated \\u003Cspan class=\\u0022route-type\\u0022\\u003E[message:field-node-ref:field_route_type]\\u003C\\/span\\u003E route \\u003Ca class=\\u0022route-title\\u0022 href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E.",
          "format" : "climbnz_full_html_no_breaks",
          "safe_value" : "[message:user:picture] \\u003Ca class=\\u0022author-link\\u0022 href=\\u0022[message:user:url:relative]\\u0022\\u003E[message:user:name]\\u003C\\/a\\u003E updated \\u003Cspan class=\\u0022route-type\\u0022\\u003E[message:field-node-ref:field_route_type]\\u003C\\/span\\u003E route \\u003Ca class=\\u0022route-title\\u0022 href=\\u0022[message:field-node-ref:url:relative]\\u0022\\u003E[message:field-node-ref:title]\\u003C\\/a\\u003E."
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $items;
}
