<?php

class ClimbNZPageMigration extends DrupalNode6Migration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Preserve the nids from D6.
    $this->addFieldMapping('nid', 'nid');
    //$this->addFieldMapping('vid', 'vid');
    $this->addFieldMapping('is_new')->defaultvalue(TRUE);

    $this->addFieldMapping('body:format', 'format')
      ->defaultValue('full_html');

    // D6 Attachments (upload) to File field.
    $this->addFieldMapping('field_file_attachments', 'upload')
        ->sourceMigration('File');
    $this->addFieldMapping('field_file_attachments:file_class')
        ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_file_attachments:preserve_files')
        ->defaultValue(TRUE);
    $this->addFieldMapping('field_file_attachments:title', 'upload:title');
    $this->addFieldMapping('field_file_attachments:description', 'upload:description');
    $this->addFieldMapping('field_file_attachments:display', 'upload:list');
    $this->addFieldMapping('field_file_attachments:language')
        ->defaultValue(LANGUAGE_NONE);

    $this->addUnmigratedSources(array(
      'revision',
      'log',
      'upload:weight',
      'revision_uid',
      'daycount',
      'timestamp',
      'totalcount',
    ));

    $this->addUnmigratedDestinations(array(
      'field_image:alt',
    ));
  }

  public function prepareRow($row) {

    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    return TRUE;
  }

  public function complete($node, stdClass $source_row) {

  }

}
