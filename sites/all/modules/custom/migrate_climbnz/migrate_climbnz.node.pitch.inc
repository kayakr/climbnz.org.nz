<?php

// drush -u 1 mi Pitch --limit=1 --force --rollback

class ClimbNZPitchMigration extends DrupalNode6Migration {
//class ClimbNZPitchMigration extends DrupalMigration {
  /**
   * Override query for basic node fields from Drupal 6 to ensure Route is published.
   * Generally unpublished Routes are spam suppressed by Mollom.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select('node', 'n')
      ->fields('n', array('nid', 'vid', 'language', 'title',
        'uid', 'status'))
      ->condition('n.type', $this->sourceType)
      ->orderBy($this->newOnly ? 'n.nid' : 'n.changed');
    //$query->innerJoin('node_revisions', 'nr', 'n.vid=nr.vid');
    //$query->fields('nr', array('body', 'teaser', 'format'));
    // Pitch field is multivalued, so we get delta as index from nid.
    $query->join('content_field_pitch', 'p', 'n.vid=p.vid');
    $query->fields('p', array(
      'delta',
      'field_pitch_grade_ewbank',
      'field_pitch_grade_alpine_technical',
      'field_pitch_grade_alpine_commitment',
      'field_pitch_grade_aid',
      'field_pitch_grade_water_ice',
      'field_pitch_grade_mixed',
      'field_pitch_grade_boulder',
      'field_pitch_bolts',
      'field_pitch_trad',
      'field_pitch_length',
      'field_pitch_description',
      'field_pitch_grade_alpine_mtcook',
    ));
    // Drop routes that have no Pitch fields.
    $query->isNotNull('p.field_pitch_description');
    $query->addExpression("CONCAT(n.nid, '-', p.delta)", 'identifier');

    return $query;
  }

  // We need to define our custom key, and populate it again in prepareRow().
  /*public function prepareKey($source_key, $row) {
    $key = array();
    foreach ($source_key as $field_name => $field_schema) {
      if ($field_name == 'identifier') {
        $key[$field_name] = $row->nid . '-' . $row->delta;
      }
    }
    return $key;
  }
  */

  public function __construct(array $arguments) {
    // Don't inherit from constructor because we need to track non-integer ids
    // defined in DrupalMigration
    //parent::__construct($arguments);

    // from DrupalMigration but nid is now identifier.
    $this->destinationType = $arguments['destination_type'];
    $this->sourceType = $arguments['source_type'];
    if (!empty($arguments['user_migration'])) {
      $user_migration = $arguments['user_migration'];
      $this->dependencies[] = $user_migration;
    }
    if (!empty($arguments['default_language'])) {
      $this->defaultLanguage = $arguments['default_language'];
    }
    //parent::__construct($arguments);
    // Need to call statically because we want to override DrupalNode6Migration and
    // DrupalMigration constructors.
    module_load_include('inc', 'migrate_d2d', 'migrate_d2d.migrate');
    DrupalMigration::__construct($arguments);

    // Document known core fields
    $this->sourceFields += array(
      'identifier' => t('Identifier'),
      'title' => t('Title'),  // @todo Depends on has_title, and label may be customized
      'body' => t('Body'),

    );

    $this->sourceFields += $this->version->getSourceFields('node', $this->sourceType);
    if ($this->moduleExists('path')) {
      $this->sourceFields['path'] = t('Path alias');
    }

    $this->destination = new MigrateDestinationNode($this->destinationType);

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'n'
        ),
        'delta' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'p',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    if (!$this->newOnly) {
      $this->highwaterField = array(
        'name' => 'nid',
        'alias' => 'n',
        'type' => 'int',
      );
    }

    // Setup common mappings
    //$this->addSimpleMappings(array('title', 'status', 'created', 'changed',
    //  'comment', 'promote', 'sticky'));

    $this->addUnmigratedSources(array(
      'changed',
      'vid',
    ));
    $this->addUnmigratedDestinations(array(
      'changed',
      'is_new',
      'revision',
      'revision_uid',
      'log'));

    if (isset($arguments['default_uid'])) {
      $default_uid = $arguments['default_uid'];
    }
    else {
      $default_uid = 1;
    }
    if (isset($user_migration)) {
      $this->addFieldMapping('uid', 'uid')
        ->sourceMigration($user_migration)
        ->defaultValue($default_uid);
    }
    else {
      $this->addFieldMapping('uid')
        ->defaultValue($default_uid);
    }

    /*if ($this->moduleExists('path')) {
      $this->addFieldMapping('path', 'path')
        ->description('Handled in prepareRow');
    }

    if (module_exists('pathauto')) {
      $this->addFieldMapping('pathauto')
        ->description('By default, disable in favor of migrated paths')
        ->defaultValue(0);
    }*/

    // From DrupalNode6Migration
    $query = $this->query();

    $this->sourceOptions['fix_field_names'] = $this->fixFieldNames;

    $this->source = new MigrateDrupal6SourceSQL($query, $this->sourceFields, NULL,
      $this->sourceOptions);


    //$this->addFieldMapping('field_published_date', 'field_published');

    // Preserve the nids from D6.
    //$this->addFieldMapping('nid', 'nid');
    //$this->addFieldMapping('vid', 'vid');
    $this->addFieldMapping('is_new')->defaultvalue(TRUE);

    // Title is generated in prepareRow().
    // @todo: consider autogenerating, i.e. for migrations and later manual additions.
    $this->addFieldMapping('title', 'title');

    $this->addFieldMapping('status')
      ->defaultvalue(TRUE);

    // Type is mapped from taxonomy term in prepareRow().
    //$this->addFieldMapping('field_route_type', 'field_type');

    $this->addFieldMapping('field_description', 'field_pitch_description');
    //$this->addFieldMapping('field_description:format', NULL, FALSE)
    //  ->defaultValue('full_html');
    $this->addFieldMapping('field_description:format', 'field_description:format')
        ->callbacks(array($this, 'mapFormat'))
        ->defaultValue('full_html');

    $this->addFieldMapping('field_grade_ewbank', 'field_pitch_grade_ewbank');
    $this->addFieldMapping('field_grade_alpine_technical', 'field_pitch_grade_alpine_technical');
    $this->addFieldMapping('field_grade_alpine_commitment', 'field_pitch_grade_alpine_commitment');
    $this->addFieldMapping('field_grade_alpine_mt_cook', 'field_pitch_grade_alpine_mtcook');

    $this->addFieldMapping('field_grade_aid', 'field_pitch_grade_aid');
    $this->addFieldMapping('field_grade_water_ice', 'field_pitch_grade_water_ice');
    $this->addFieldMapping('field_grade_mixed', 'field_pitch_grade_mixed');
    $this->addFieldMapping('field_grade_boulder', 'field_pitch_grade_boulder');

    $this->addFieldMapping('field_length', 'field_pitch_length');
    $this->addFieldMapping('field_bolts', 'field_pitch_bolts');
    $this->addFieldMapping('field_natural_pro', 'field_pitch_trad');
  }

  public function prepareRow($row) {
    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->identifier = $row->nid . '-' . $row->delta;
    $row->title = $row->identifier;

    // Default values?
    $row->sourceid1 = $row->nid;
    $row->sourceid2 = $row->delta;

    xdebug_break();
  }

}
