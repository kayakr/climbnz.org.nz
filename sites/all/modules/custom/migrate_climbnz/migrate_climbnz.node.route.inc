<?php

class ClimbNZRouteMigration extends DrupalNode6Migration {

  /**
   * Override query for basic node fields from Drupal 6 to ensure Route is published.
   * Generally unpublished Routes are spam suppressed by Mollom.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $query = parent::query();
    // Screen for published nodes, as unpublished are likely spam.
    $query->condition('n.status', 1);
    return $query;
  }

  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->addFieldMapping('field_published_date', 'field_published');

    // Preserve the nids from D6.
    $this->addFieldMapping('nid', 'nid');
    // Have disabled migrating original vid due to collisions that prevent nodes migrating.
    //$this->addFieldMapping('vid', 'vid');
    $this->addFieldMapping('is_new')->defaultvalue(TRUE);

    // Type is mapped from taxonomy term in prepareRow().
    $this->addFieldMapping('field_route_type', 'field_type');

    $this->addFieldMapping('field_description', 'body')
      ->callbacks(array($this, 'mapDescription'));
    $this->addFieldMapping('field_description:format', 'field_description:format')
        ->callbacks(array($this, 'mapFormat'))
        ->defaultValue('full_html');

    $this->addFieldMapping('field_reference', 'field_reference');

    $this->addFieldMapping('field_grade', 'field_grade')
      ->callbacks(array($this, 'mapGrade'));

    // field_pitch
    $this->addFieldMapping('field_pitch', 'field_pitch');
    $this->addFieldMapping('field_pitch:grade_ewbank', 'field_pitch:grade_ewbank');
    $this->addFieldMapping('field_pitch:grade_alpine_technical', 'field_pitch:grade_alpine_technical');
    $this->addFieldMapping('field_pitch:grade_alpine_commitment', 'field_pitch:grade_alpine_commitment');
    $this->addFieldMapping('field_pitch:grade_alpine_mtcook', 'field_pitch:grade_alpine_mtcook');
    $this->addFieldMapping('field_pitch:grade_aid', 'field_pitch:grade_aid');
    $this->addFieldMapping('field_pitch:grade_water_ice', 'field_pitch:grade_water_ice');
    $this->addFieldMapping('field_pitch:grade_mixed', 'field_pitch:grade_mixed');
    $this->addFieldMapping('field_pitch:grade_boulder', 'field_pitch:grade_boulder');

    $this->addFieldMapping('field_pitch:length', 'field_pitch:length');
    $this->addFieldMapping('field_pitch:bolts', 'field_pitch:bolts');
    $this->addFieldMapping('field_pitch:trad', 'field_pitch:trad');
    $this->addFieldMapping('field_pitch:description', 'field_pitch:description');

    $this->addFieldMapping('field_ascent', 'field_ascent');

    $this->addFieldMapping('field_attribution', 'field_attribution');

    $this->addFieldMapping('field_quality', 'field_quality');

    $this->addFieldMapping('field_gone', 'field_gone')
      ->callbacks(array($this, 'mapBoolean'));

    $this->addFieldMapping('field_length', 'field_length');

    $this->addFieldMapping('field_bolts', 'field_bolts');

    $this->addFieldMapping('field_natural_pro', 'field_natural')
      ->callbacks(array($this, 'mapBoolean'));

    // D6 Image field.
    $this->addFieldMapping('field_route_image', 'field_route_image')
      ->sourceMigration('File');
    $this->addFieldMapping('field_route_image:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_route_image:preserve_files')
      ->defaultValue(TRUE);
    //$this->addFieldMapping('field_image:title', 'field_image:title');
    //$this->addFieldMapping('field_image_description', 'field_image:description');
    $this->addFieldMapping('field_route_image:title', 'field_route_image:title');
    $this->addFieldMapping('field_route_image:display', 'field_route_image:list');
    $this->addFieldMapping('field_route_image:language')
      ->defaultValue(LANGUAGE_NONE);
  }

  public function getPitchByTitle($title) {
    $result = db_query("SELECT nid FROM {node} WHERE title=:title", array(':title' => $title))->fetchField();
    return $result;
  }

  // Strip tags, e.g. from node/188
  // <P><SPAN lang=EN-AU style="FONT-SIZE: 10 etc.
  public function mapDescription($value) {
    $value = drupal_html_to_text($value);
    $value = trim($value);
    return $value;
  }

  /**
   * Boulder pitch grade (e.g. VM) has been propagated to field_grade as index, e.g. 5.
   * @param $value
   *
   * @return string
   */
  public function mapGrade($value) {
    $route = $this->source->current();
    if ($route->field_type == 'boulder' && is_numeric($route->field_grade)) {
      $boulder_options = climbnz_pitch_grade_boulder_options();
      $value = isset($boulder_options[$value]) ? $boulder_options[$value] : $value;
    }
    
    return trim($value);
  }

  /**
   * Map yes|no to 1|0 for boolean field, used for checkbox display.
   * @param $value
   *
   * @return int
   */
  public function mapBoolean($value) {
    if ($value == 'yes') {
      return 1;
    }
    return 0;
  }

  public function prepareRow($row) {
    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
  }

  public function complete($node, stdClass $source_row) {
    //xdebug_break();
  }

}
