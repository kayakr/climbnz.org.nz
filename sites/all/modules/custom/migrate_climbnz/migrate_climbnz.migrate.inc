<?php
/**
* @file
* Declares our migrations.
*/


/**
* Implements hook_migrate_api().
*/
function migrate_climbnz_migrate_api() {
  global $conf;

  $api = array(
    'api' => 2,
    'groups' => array(
      'climbnz' => array(
        'title' => t('ClimbNZ Migrations'),
      ),
    ),
    'migrations' => array(),
    // Register a custom field handler for Pitches.
    'field handlers' => array('MigratePitchFieldHandler'),
  );

  $common_arguments = array(
    'source_connection' => 'legacy',
    'source_version' => 6,
    'group_name' => 'climbnz',
    'format_mappings' => array(
      '1' => 'filtered_html',
      '2' => 'full_html',
      '3' => 'filtered_html', // @todo: Rich text
    ),
  );

  $api['migrations']['Role'] = $common_arguments + array(
    'description' => t('Import Drupal 6 roles'),
    'class_name' => 'DrupalRole6Migration',
    'role_mappings' => array(
      'Site Admin' => 'administrator',
      'Admin' => 'administrator',
    ),
  );

  // Register the user migration.
  // We just use the migrate_d2d D6 migration class as-is.
  $api['migrations']['User'] = $common_arguments + array(
    'description' => t('Migration of users from Drupal 6'),
    'class_name' => 'ClimbNZDrupalUser6Migration',
    'role_migration' => 'Role',
  );

  // Per https://www.drupal.org/node/1819716
  // drush mi Menu --idlist='menu-places'
  $api['migrations']['Menu'] = $common_arguments + array(
    'description' => t('Migrate Place menu from Drupal 6'),
    'machine_name' => 'menu-places',
    'class_name' => 'DrupalMenu6Migration', // Default class.
  );

  $api['migrations']['MenuLinks'] = $common_arguments + array(
    'machine_name' => 'menu-places-link',
    'description' => t('Import Drupal 6 menu links for Places'),
    'menu_migration' => 'menu-places', // see Menu above.
    'node_migrations' => array('Place'),
    'class_name' => 'DrupalMenuLinks6Migration',
  );
  // Do we still need to create

  // Migrate Files from legacy public to D7 public dir.
  // These generate an error message when migrating.
  $file_arguments = $common_arguments + array(
      'class_name' => 'ClimbNZDrupalFile6Migration',
      'description' => t('Import Drupal 6 files'),
      'user_migration' => 'User',
      'default_uid' => 1,
      'source_dir' => $conf['migrate_climbnz_source_dir'], // '/Users/jonathan/Sites/drupal-6.38',
      'destination_dir' => 'public://',
    );
  $api['migrations']['File'] = $file_arguments;


  // Node migrations - each has its own class derived from the migrate_d2d class,
  // specifying its particular field mappings and transformations. source_type
  // and destination_type are required arguments.
  $node_arguments = array(
    //'Image' => array(
    //  'class_name' => 'ExampleImageMigration',
    //  'description' => t('Migration of image nodes from Drupal 6'),
    //  'source_type' => 'image',
    //  'destination_type' => 'image',
    //),
    'Guide' => array(
      'class_name' => 'ClimbNZGuideMigration',
      'description' => t('Migration of Guide nodes from Drupal 6'),
      'source_type' => 'guide',
      'destination_type' => 'guide',
    ),
    'Page' => array(
      'class_name' => 'ClimbNZPageMigration',
      'description' => t('Migration of Page nodes from Drupal 6'),
      'source_type' => 'page',
      'destination_type' => 'page',
    ),
    'Place' => array(
      'class_name' => 'ClimbNZPlaceMigration',
      'description' => t('Migration of Place nodes from Drupal 6'),
      'source_type' => 'place',
      'destination_type' => 'place',
      'dependencies' => array('File'),
    ),
    'PlaceRevision' => array(
      'class_name' => 'ClimbNZPlaceRevisionMigration',
      'description' => t('Migration of Place node revisions from Drupal 6'),
      'source_type' => 'place',
      'destination_type' => 'place',
      'dependencies' => array('File', 'Guide'),
    ),
    /*'Pitch' => array(
      'class_name' => 'ClimbNZPitchMigration',
      'description' => t('Migration of Pitch fields from Drupal 6'),
      'source_type' => 'route',
      'destination_type' => 'pitch',
      //'dependencies' => array('Place'),
    ),*/
    'Route' => array(
      'class_name' => 'ClimbNZRouteMigration',
      'description' => t('Migration of Route nodes from Drupal 6'),
      'source_type' => 'route',
      'destination_type' => 'route',
      'dependencies' => array('Place'), // add 'Pitch' if we use nodes for Pitches.
    ),
  );

  // Tell the node migrations where the users are coming from, so they can
  // set up the dependency and resolve D6->D7 uids.
  $common_node_arguments = $common_arguments + array(
      'user_migration' => 'User'
    );

  foreach ($node_arguments as $migration_name => $arguments) {
    $arguments = array_merge_recursive($arguments, $common_node_arguments);
    $api['migrations'][$migration_name] = $arguments;
  }

  // Comment migration.
  $comment_arguments = $common_arguments + array(
      'user_migration' => 'User',
      'default_uid' => 1,
    );
  $place_comment_arguments = $comment_arguments + array(
      'machine_name' => 'PlaceComment',
      'class_name' => 'ClimbNZPlaceCommentDrupalComment6Migration',
      'description' => t('Import Drupal 6 Place comments'),
      'source_type' => 'place',
      'destination_type' => 'place',
      'node_migration' => 'Place',
  );
  $api['migrations']['PlaceComment'] = $place_comment_arguments;

  $route_comment_arguments = $comment_arguments + array(
      'machine_name' => 'RouteComment',
      'class_name' => 'ClimbNZRouteCommentDrupalComment6Migration',
      'description' => t('Import Drupal 6 Route comments'),
      'source_type' => 'route',
      'destination_type' => 'route',
      'node_migration' => 'Route',
    );
  $api['migrations']['RouteComment'] = $route_comment_arguments;

  return $api;
}

/**
 * Class MigratePitchFieldHandler
 * from https://www.drupal.org/node/1429096
 */
class MigratePitchFieldHandler extends MigrateFieldHandler {
  public function __construct() {
    // This handler will take care of all the simple core field types
    $this->registerTypes(array('pitch'));
  }
  
  public function prepare($entity, array $field_info, array $instance, array $values) {
    $migration = Migration::currentMigration();
    $arguments = (isset($values['arguments']))? $values['arguments']: array();
    // In Drupal 7, field values are indexed by language, which can be specified
    // at various levels - this member implemented in MigrateFieldHandler works
    // out which language to use, applying appropriate defaults
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);
    // Setup the standard Field API array for saving.
    $delta = 0;

    // @todo: for some reason grade ewbank arrives as base value, not indexed in arguments.
    // Unset it from arguments so it doesn't replace integer value with null.
    //unset($values['arguments']['grade_ewbank']);
    unset($arguments['grade_ewbank']);

    foreach ($values as $index => $value) {
      if (is_integer($index)) {
        $pitch = array('grade_ewbank' => $value);
        foreach ($arguments as $key => $source_value) {
          $pitch[$key] = $source_value[$delta];
        }
        $return[$language][$delta] = $pitch;
        $delta++;
      }
    }
    if (!isset($return)) {
      $return = NULL;
    }
    return $return;
  }
}
