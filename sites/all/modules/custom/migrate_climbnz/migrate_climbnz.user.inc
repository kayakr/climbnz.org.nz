<?php

class ClimbNZDrupalUser6Migration extends DrupalUser6Migration {

  public function __construct(array $arguments) {
    //$this->dependencies[] = 'Role';

    parent::__construct($arguments);

    $this->addFieldMapping('roles', 'roles')
      ->sourceMigration('Role');

    $this->addFieldMapping('is_new')->defaultvalue(TRUE);
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('field_firstname', 'profile_firstname');
    //$this->addfieldMapping('field_first_name:language')->defaultValue('en');
    $this->addFieldMapping('field_lastname', 'profile_lastname');
    //$this->addfieldMapping('field_last_name:language')->defaultValue('en');
    $this->addFieldMapping('field_contact_id', 'profile_contact_id');

    $this->addFieldMapping('signature', 'signature')
      ->callbacks(array($this, 'truncateSignature'));
  }

  public function truncateSignature($value) {
    return substr($value, 0, 255);
  }

  public function prepareRow($row) {
    //flog_it(__CLASS__ .'->'. __FUNCTION__ . ': row=' . print_r($row, TRUE));
    //drush_print_r($row);

    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // We need to populate profile fields from d6 db.
    $profile_fields = Database::getConnection('default', 'legacy')
      ->select('profile_values', 'pv')
      ->fields('pv', array('fid', 'value'))
      ->condition('uid', $row->uid)
      ->execute()
      ->fetchAllKeyed();
    //xdebug_break();
    $row->profile_firstname = $profile_fields[1];
    $row->profile_lastname = $profile_fields[2];
    $row->profile_contact_id = $profile_fields[3];

    return TRUE;
  }
}
