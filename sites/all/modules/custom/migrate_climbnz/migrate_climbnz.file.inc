<?php


class ClimbNZDrupalFile6Migration extends DrupalFile6Migration {

  public function __construct(array $arguments) {
    $this->newOnly = true;

    parent::__construct($arguments);

    //$this->destination = new MigrateDestinationNodeRevision($arguments['destination_type']);

  }

  public function prepareRow($row) {

    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // $row->filename = "_original"
    // Ignore D6 derivative files.
    if (in_array($row->filename, array('tiny', 'thumbnail', 'medium', 'preview'))) {
      return FALSE;
    }

    // ignore spam images.
    if (strpos($row->filename, 'pharma2_') !== FALSE) {
      return FALSE;
    }

    // @todo: check if file exists?
    // if not, make a complete path using $conf['migrate_climbnz_source_dir']
    if (!file_exists($row->filepath)) {
      $filepath_new = $GLOBALS['conf']['migrate_climbnz_source_dir'] . $row->filepath;
      if(file_exists($filepath_new)) {
        $row->filepath = $filepath_new;
      }
    }

    return TRUE;
  }

}
