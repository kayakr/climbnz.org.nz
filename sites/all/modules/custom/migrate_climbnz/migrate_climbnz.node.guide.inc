<?php

class ClimbNZGuideMigration extends DrupalNode6Migration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Preserve the nids from D6.
    $this->addFieldMapping('nid', 'nid');
    //$this->addFieldMapping('vid', 'vid');
    $this->addFieldMapping('is_new')->defaultvalue(TRUE);

    $this->addFieldMapping('field_year_of_publication', 'field_year');
    $this->addFieldMapping('field_year_of_publication:timezone')
      ->defaultValue('Pacific/Auckland');

    $this->addFieldMapping('field_link', 'field_link');
    //field_link:title	Subfield: The link title attribute
    $this->addFieldMapping('field_link:title', 'field_link:title');
    //field_link:language	Subfield: The language for the field
    $this->addFieldMapping('field_link:language')
      ->defaultValue('en');

    // D6 Attachments (upload) to File field.
    $this->addFieldMapping('field_cover', 'field_cover')
      ->sourceMigration('File');
    $this->addFieldMapping('field_cover:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_cover:preserve_files')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_cover:title', 'field_cover:title');
    $this->addFieldMapping('field_cover:alt', 'field_cover:alt');
    $this->addFieldMapping('field_cover:description', 'field_cover:description');
    $this->addFieldMapping('field_cover:display', 'field_cover:list');
    $this->addFieldMapping('field_cover:language')
      ->defaultValue(LANGUAGE_NONE);

    /*
    $this->addFieldMapping('body:format', 'format')
      ->defaultValue('full_html');
*/
    $this->addUnmigratedSources(array(
      'revision',
      'log',
      'upload:weight',
      'revision_uid',
      'daycount',
      'timestamp',
      'totalcount',
      'field_link:attributes',
    ));

    $this->addUnmigratedDestinations(array(
      'field_year_of_publication:rrule',
      'field_year_of_publication:to',
      'field_link:attributes',
    ));
  }

  public function prepareRow($row) {

    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    return TRUE;
  }

  public function complete($node, stdClass $source_row) {

  }

}
