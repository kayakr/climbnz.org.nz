<?php

class ClimbNZPlaceMigration extends DrupalNode6Migration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Preserve the nids from D6.
    $this->addFieldMapping('nid', 'nid');
    //$this->addFieldMapping('vid', 'vid');
    $this->addFieldMapping('is_new')->defaultvalue(TRUE);

    // Type is mapped from taxonomy term in prepareRow().
    $this->addFieldMapping('field_type', 'field_place_type')
      ->callbacks(array($this, 'mapType'));

    $this->addFieldMapping('field_description', 'field_description');
    //$this->addFieldMapping('field_description:format', NULL, FALSE)
    //  ->defaultValue('full_html');
    $this->addFieldMapping('field_description:format', 'field_description:format')
      ->callbacks(array($this, 'mapFormat'))
      ->defaultValue('full_html');

    $this->addFieldMapping('field_altitude', 'field_height');

    // Aspect is mapped from taxonomy term in prepareRow().
    $this->addFieldMapping('field_aspect', 'field_aspect');

    $this->addFieldMapping('field_access', 'field_access');
    $this->addFieldMapping('field_access:format', 'field_access:format')
      ->callbacks(array($this, 'mapFormat'))
      ->defaultValue('filtered_html');
    $this->addFieldMapping('field_access_private', 'field_access_private');
    $this->addFieldMapping('field_access_private:format') //, 'field_access_private:format')
      ->defaultValue('filtered_html');

    $this->addFieldMapping('field_attribution', 'field_attribution');

    $this->addFieldMapping('field_walktime', 'field_walktime');

    $this->addFieldMapping('field_guide', 'field_guide');

    // Map from D6 WKT point value to D7 Lat/Lon.
    $this->addFieldMapping('field_geo_lat_lon', 'field_geo_latlon');
    $this->addFieldMapping('field_geo_lat_lon:geo_type')->defaultValue('point');
    $this->addFieldMapping('field_geo_lat_lon:input_format')->defaultValue('lat/lon');
    $this->addFieldMapping('field_geo_lat_lon:lat', 'lat');
    $this->addFieldMapping('field_geo_lat_lon:lon', 'lon');
    $this->addFieldMapping('field_nzms260', 'field_nzms260');
    $this->addFieldMapping('field_topo50', 'field_topo50');


    // D6 Image field.
    $this->addFieldMapping('field_image', 'field_image')
        ->sourceMigration('File');
    $this->addFieldMapping('field_image:file_class')
        ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_image:preserve_files')
        ->defaultValue(TRUE);
    //$this->addFieldMapping('field_image:title', 'field_image:title');
    //$this->addFieldMapping('field_image_description', 'field_image:description');
    $this->addFieldMapping('field_image:title', 'field_image_data_title');
    $this->addFieldMapping('field_image:display', 'field_image:list');
    $this->addFieldMapping('field_image:language')
        ->defaultValue(LANGUAGE_NONE);

    // D6 Attachments (upload) to File field.
    $this->addFieldMapping('field_file_attachments', 'upload')
        ->sourceMigration('File');
    $this->addFieldMapping('field_file_attachments:file_class')
        ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_file_attachments:preserve_files')
        ->defaultValue(TRUE);
    $this->addFieldMapping('field_file_attachments:title', 'upload:title');
    $this->addFieldMapping('field_file_attachments:description', 'upload:description');
    $this->addFieldMapping('field_file_attachments:display', 'upload:list');
    $this->addFieldMapping('field_file_attachments:language')
        ->defaultValue(LANGUAGE_NONE);

    $this->addUnmigratedSources(array(
      'field_geo_lat_lon:bottom',
      'revision',
      'log',
      'upload:weight',
    ));

    $this->addUnmigratedDestinations(array(
      'field_geo_lat_lon:wkt',
      'field_geo_lat_lon:json',
      'field_geo_lat_lon:left',
      'field_geo_lat_lon:right',
      'field_geo_lat_lon:top',
      'field_image:alt',
    ));
  }

  // #41 Change National Park to Protected Area.
  public function mapType($value) {
    if ($value == 'landfeature') {
      $value = 'feature';
    }
    if ($value == 'national_park') {
      $value = 'protected_area';
    }
    return $value;
  }

  public function prepareRow($row) {

    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Look up point data from D6 content_type_place, pass as lat, lon for field mapping.
    $query = Database::getConnection('default', 'legacy')
      ->select('content_type_place', 'p')
      ->fields('p', array('field_geo_latlon_geo'))
      ->condition('vid', $row->vid);
    $query->addExpression('AsText(field_geo_latlon_geo)', 'field_geo');
    $wkt = $query->execute()->fetchField(1); // Target the field_geo alias.
    $file = geophp_load();
    $geom = geoPHP::load($wkt, 'wkt');
    $row->lat = $geom->coords[1];
    $row->lon = $geom->coords[0];

    // Look up Type term name, map to key of enumerated list.
    $type_tid = $row->field_place_type;
    $type = Database::getConnection('default', 'legacy')
      ->select('term_data', 't')
      ->fields('t', array('name'))
      ->condition('tid', $type_tid)
      ->execute()
      ->fetchField();
    $row->field_place_type = $this->_convertToTextKey($type);

    // Look up Aspect term name, map to key of enumerated list.
    if (!empty($row->field_aspect)) {
      $aspect = Database::getConnection('default', 'legacy')
        ->select('term_data', 't')
        ->fields('t', array('name'))
        ->condition('tid', $row->field_aspect)
        ->execute()
        ->fetchField();
      $row->field_aspect = $this->_convertToTextKey($aspect);
    }

    // Look up image metadata.
    if (isset($row->field_image)) {
      $image = Database::getConnection('default', 'legacy')
        ->select('content_field_image', 'f')
        ->fields('f')
        ->condition('vid', $row->vid)
        ->execute()
        ->fetchAllAssoc('vid');
      if (isset($image)) {
        $image_data = unserialize($image[$row->vid]->field_image_data);
        // If title and description are the same, use title,
        if ($image_data['description'] == $image_data['title']) {
          $row->field_image_data_title = $image_data['title'];
        }
        // otherwise use either or both.
        else {
          $row->field_image_data_title = trim($image_data['title'] . ' ' . $image_data['description']);
        }
      }
    }

    /*if (isset($row->field_image)) {
      $fid = $row->field_image[0];
      $file = Database::getConnection('default', 'legacy')
        ->select('files', 'f')
        ->fields('f')
        ->condition('fid', $fid)
        ->execute()
        ->fetchAllAssoc('fid');
    }*/

    // Migrate hierarchy entry/entries.
    /*$mlid = Database::getConnection('default', 'legacy')
      ->select('nodehierarchy_menu_links', 'ml')
      ->fields('ml', array('mlid'))
      ->condition('nid', $row->nid)
      ->execute()
      ->fetchField();
      if (empty($mlid)) {
         drupal_set_message(t('No node hierarchy mlid found for node @nid', array('@nid' => $row->nid)), 'error');
      }
      else {
        // Place could have multiple parents, e.g. node/31 Mt Cook Range
        // has parents Aoraki (node/781), Hooker Glacier (node/6845), Ball Glacier (node/6848).

        $menu_link =  Database::getConnection('default', 'legacy')
            ->select('menu_links', 'ml')
            ->fields('ml', array('plid', 'link_path', 'router_path', 'link_title', 'module', 'hidden', 'weight', 'depth', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'))
            ->condition('mlid', $mlid)
            ->execute()
            ->fetchAssoc();
      }
      */

    return TRUE;
  }

  // e.g. National Park to national_park.
  function _convertToTextKey($value) {
    return strtolower(str_replace(array(' ', '-'), '_', $value));
  }

  public function complete($node, stdClass $source_row) {

  }

}
