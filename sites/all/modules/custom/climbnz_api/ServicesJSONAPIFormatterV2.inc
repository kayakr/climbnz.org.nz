<?php
/**
 * Implement ServicesFormatterInterface to enable JSONAPI (vnd.api+json) responses.
 */
class ServicesJSONAPIFormatterV2 implements ServicesFormatterInterface {

  public function render($output) {
    // If the handler has returned an error, $output is a string.
    // json_encode doesn't give valid json with data that isn't an array/object.
    if (is_scalar($output)) {
      $error = new stdClass();
      $error->detail = $output;
      return json_encode(array('errors' => array($error)));
    }
    // Output version info.
    $output['meta']['version'] = '2';
    $output['jsonapi'] = array(
      'version' => '1.1',
    );

    return str_replace('\\/', '/', json_encode($output));
  }

}
