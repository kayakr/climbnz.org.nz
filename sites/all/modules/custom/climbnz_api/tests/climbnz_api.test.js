module.exports = {
  'Test ClimbNZ API' : function (browser) {
    browser
        .url('https://climbnz.org.nz/api')
        .waitForElementVisible('body')
        //.setValue('input[type=text]', 'nightwatch')
        //.waitForElementVisible('input[name=btnK]')
        //.click('input[name=btnK]')
        //.pause(1000)
        .assert.containsText('h1', 'ClimbNZ API')
        .end();
  }
};
