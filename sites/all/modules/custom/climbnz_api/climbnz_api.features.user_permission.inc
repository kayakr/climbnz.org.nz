<?php

/**
 * @file
 * climbnz_api.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function climbnz_api_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer oauth2 server'.
  $permissions['administer oauth2 server'] = array(
    'name' => 'administer oauth2 server',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'oauth2_server',
  );

  // Exported permission: 'administer services'.
  $permissions['administer services'] = array(
    'name' => 'administer services',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'services',
  );

  // Exported permission: 'use oauth2 server'.
  $permissions['use oauth2 server'] = array(
    'name' => 'use oauth2 server',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'oauth2_server',
  );

  return $permissions;
}
