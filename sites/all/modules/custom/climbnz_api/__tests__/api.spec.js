var frisby = require('frisby');

var baseUrl = 'https://climbnz.org.nz';
//var baseUrl = 'http://climbnz7.local';

// Do setup first
frisby.globalSetup({
  request: {
    headers: {
      'Accept': 'application/vnd.api+json'
    }
  }
});

it('GET places returns HTTP 200', function () {
  // Return the Frisby.js Spec in the 'it()' (just like a promise)
  return frisby.get(baseUrl + '/api/places')
      .expect('status', 200);
});

it('GET places returns application/vnd.api+json', function () {
  return frisby
      .get(baseUrl + '/api/places')
      .expect('header', 'Content-Type', 'application/vnd.api+json')
      .expect('json', 'jsonapi.version', '1.1')
});

it('GET places index has places', function () {
  return frisby
      .get(baseUrl + '/api/places')
      .expect('json', 'data[0]type', 'place')
});

it('GET places index has pagination', function () {
  return frisby
      .get(baseUrl + '/api/places')
      .expect('json', 'links.first', /page\[offset\]/) // regex
      .expect('json', 'links.last', /page/) // regex
});

it('GET places index can be filtered by placeType', function () {
  return frisby
      .get(baseUrl + '/api/places?filter[placeType]=mountain')
      .expect('json', 'data.*.attributes', {
        'placeType': 'mountain'
      })
});

it('GET Aoraki api/places/9e56903e-d6aa-48b2-a0ab-840bb4f725b5 is Aoraki Mt Cook', function () {
  return frisby
      .get(baseUrl + '/api/places/9e56903e-d6aa-48b2-a0ab-840bb4f725b5')
      .expect('status', 200)
      .expect('header', 'Content-Type', 'application/vnd.api+json')
      .expect('json', 'data.type', 'place')
      .expect('json', 'data.attributes.name', 'Aoraki Mt Cook')
});

// Routes
it('GET routes returns application/vnd.api+json', function () {
  return frisby
      .get(baseUrl + '/api/routes')
      .expect('status', 200)
      .expect('header', 'Content-Type', 'application/vnd.api+json')
      .expect('json', 'jsonapi.version', '1.1')
});

it('GET routes index has routes', function () {
  return frisby
      .get(baseUrl + '/api/routes')
      .expect('json', 'data[0]type', 'route')
});

// Guides
it('GET guides returns application/vnd.api+json', function () {
  return frisby
      .get(baseUrl + '/api/guides')
      .expect('status', 200)
      .expect('header', 'Content-Type', 'application/vnd.api+json')
      .expect('json', 'jsonapi.version', '1.1')
});

it('GET guides index has guides', function () {
  return frisby
      .get(baseUrl + '/api/guides')
      .expect('json', 'data.*.type', 'guide')
});

it('GET Guide api/places/645deca4-3059-4558-a5a8-17adcdc2ae61 is Wellington Rock', function () {
  return frisby
      .get(baseUrl + '/api/guides/645deca4-3059-4558-a5a8-17adcdc2ae61')
      .expect('status', 200)
      .expect('header', 'Content-Type', 'application/vnd.api+json')
      .expect('json', 'data.type', 'guide')
      .expect('json', 'data.attributes.name', 'Wellington Rock')
});
