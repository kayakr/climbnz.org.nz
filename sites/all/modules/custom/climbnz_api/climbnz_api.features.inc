<?php

/**
 * @file
 * climbnz_api.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function climbnz_api_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_default_oauth2_server().
 */
function climbnz_api_default_oauth2_server() {
  $items = array();
  $items['climbnz_auth'] = entity_import('oauth2_server', '{
    "name" : "climbnz_auth",
    "label" : "climbnz_auth",
    "settings" : {
      "enforce_state" : true,
      "default_scope" : "default",
      "allow_implicit" : 1,
      "use_openid_connect" : 0,
      "use_crypto_tokens" : 0,
      "grant_types" : {
        "authorization_code" : "authorization_code",
        "client_credentials" : "client_credentials",
        "refresh_token" : "refresh_token",
        "urn:ietf:params:oauth:grant-type:jwt-bearer" : 0,
        "password" : 0
      },
      "always_issue_new_refresh_token" : 0,
      "access_lifetime" : "3600",
      "id_lifetime" : "3600",
      "refresh_token_lifetime" : "1209600",
      "require_exact_redirect_uri" : 1
    },
    "rdf_mapping" : [],
    "scopes" : [
      {
        "name" : "default",
        "description" : "Default scope",
        "rdf_mapping" : []
      }
    ]
  }');
  return $items;
}
