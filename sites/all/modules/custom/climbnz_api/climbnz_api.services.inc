<?php

/**
 * @file
 * climbnz_api.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function climbnz_api_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'climbnz_api1';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api';
  $endpoint->authentication = array(
    'oauth2_server' => array(
      'server' => 'climbnz_auth',
    ),
    'services_basic_auth' => 'services_basic_auth',
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'vnd.api+json' => TRUE,
      'json' => TRUE,
      'v1.api+json' => TRUE,
      'v2.api+json' => TRUE,
      'xml' => TRUE,
      'bencode' => FALSE,
      'hal+json' => FALSE,
      'jsonp' => FALSE,
      'php' => FALSE,
      'v0.0.1.hal+json' => FALSE,
      'v0.0.2.hal+json' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/xml' => TRUE,
      'multipart/form-data' => TRUE,
      'text/xml' => TRUE,
      'application/x-www-form-urlencoded' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'enumerations' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => '',
              'require_authentication' => NULL,
            ),
          ),
        ),
      ),
    ),
    'guides' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => '',
              'require_authentication' => NULL,
            ),
          ),
        ),
        'index' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => '',
              'require_authentication' => NULL,
            ),
          ),
        ),
      ),
    ),
    'node' => array(
      'relationships' => array(
        'comments' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => 'o',
              'require_authentication' => NULL,
            ),
          ),
        ),
      ),
    ),
    'places' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => '',
              'require_authentication' => NULL,
            ),
          ),
        ),
        'index' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => '',
              'require_authentication' => NULL,
            ),
          ),
        ),
      ),
    ),
    'routes' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => '',
              'require_authentication' => NULL,
            ),
          ),
        ),
        'index' => array(
          'enabled' => '1',
          'settings' => array(
            'oauth2_server' => array(
              'scope' => '',
              'require_authentication' => NULL,
            ),
          ),
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['climbnz_api1'] = $endpoint;

  return $export;
}
