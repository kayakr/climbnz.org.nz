<?php

/**
 * @file
 * climbnz_footer_menu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function climbnz_footer_menu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
