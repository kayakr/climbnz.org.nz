<?php
/**
 * @file
 * climbnz_footer_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function climbnz_footer_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_places:places.
  $menu_links['navigation_places:places'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'places',
    'router_path' => 'places',
    'link_title' => 'Places',
    'options' => array(
      'identifier' => 'navigation_places:places',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Places');

  return $menu_links;
}
