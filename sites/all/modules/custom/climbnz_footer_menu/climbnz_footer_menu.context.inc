<?php

/**
 * @file
 * climbnz_footer_menu.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function climbnz_footer_menu_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'climbnz_footer';
  $context->description = 'Blocks in climbnz footer region';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin' => '~admin',
        '~admin/*' => '~admin/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-footer-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-footer-menu',
          'region' => 'footer',
          'weight' => '-9',
        ),
        'climbnz-climbnz_footer_links' => array(
          'module' => 'climbnz',
          'delta' => 'climbnz_footer_links',
          'region' => 'footer',
          'weight' => '-8',
        ),
        'climbnz_theme-climbnz_catalyst_cloud' => array(
          'module' => 'climbnz_theme',
          'delta' => 'climbnz_catalyst_cloud',
          'region' => 'footer',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks in climbnz footer region');
  $export['climbnz_footer'] = $context;

  return $export;
}
