Node Hierarchy Extras

Extra functionality related to the Node Hierarchy module for Drupal 7.
The module contains the following sub-modules:

    Node Hierarchy Recursive Pathauto
    Node Hierarchy Feeds Mapper

See relevant README.txt files in submodules
