Node Hierarchy Recursive Pathauto

Recursively updates aliases provided by the Path Auto module.
When a parent node is renamed and it's alias gets updated, this module
recursively updates the aliases of all descendant nodes.
Rearranging nodes through the menu link list page is also handled
in the same way.

Required Modules:

    Node Hierarchy
    Pathauto

The code for this was initially based on an existing drupal 6 version.

Minimally tested using the pathauto pattern on a 7.14 installation:
[node:nodehierarchy:parent:url:path]/[node:title]
