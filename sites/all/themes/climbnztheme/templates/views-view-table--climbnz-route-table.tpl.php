<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 *
 * @ingroup views_templates
 */

// loop over rows, inject 'gone' as row class.
foreach ($rows as $row_index => $row) {
  if (!empty($row['field_gone'])) {
    $row_classes[$row_index][] = 'route-gone';
  }
}
?>
<table <?php if ($classes): ?> class="<?php print $classes; ?>"<?php endif ?><?php print $attributes; ?>>
  <?php if (!empty($title) || !empty($caption)): ?>
    <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>
    <thead>
    <tr>
      <?php foreach ($header as $field => $label): ?>
        <th <?php if ($header_classes[$field]): ?> class="<?php print $header_classes[$field]; ?>"<?php endif; ?> scope="col">
          <?php print $label; ?>
        </th>
      <?php endforeach; ?>
    </tr>
    </thead>
  <?php endif; ?>
  <tbody>
  <?php foreach ($rows as $row_count => $row): ?>
    <tr <?php if ($row_classes[$row_count]): ?> class="<?php print implode(' ', $row_classes[$row_count]); ?>"<?php endif; ?>>
      <?php foreach ($row as $field => $content): ?>
        <td <?php if ($field_classes[$field][$row_count]): ?> class="<?php print $field_classes[$field][$row_count]; ?>"<?php endif; ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
          <?php print $content; ?>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr class="route-description-row <?php echo $row['field_gone'] ? 'route-gone' : ''; ?>">
      <td><!--reference skip--></td>
      <td colspan="<?php echo count($header)-1; ?>">
        <div class="description"><?php
        // Output overall Route Description.
        if (!empty($view->result[$row_count]->field_field_description[0]['rendered'])) {
          echo $view->result[$row_count]->field_field_description[0]['rendered']['#markup'];
        }
        else {
          // Render description from first pitch.
          $route = $view->result[$row_count];
          $pitches = $route->field_field_pitch;
          $pitch_description = filter_xss($pitches[0]['raw']['description']);
          echo $pitch_description;
        }
        ?></div>
        <?php
        // only render pitches if > 1.
        // We use table rows from the pitches field as a proxy. First row is header row, and each
        // pitch uses two rows.
        if (!empty($view->result[$row_count]->field_field_pitch[0]['rendered'])) {
            $pitches_markup = $view->result[$row_count]->field_field_pitch[0]['rendered']['#markup'];
            $tr_count = substr_count($pitches_markup, '<tr');
            $pitch_count = ($tr_count - 1) /2;
            if ($pitch_count > 1) {
              ?>
                <div class="pitches"><?php
                  // Output 1..N Pitches as table.
                  echo $view->result[$row_count]->field_field_pitch[0]['rendered']['#markup'];
                  ?></div>
              <?php
            } // /if pitch_count
        } // if field_field_pitch
        ?>
        <div class="ascent"><em><?php
        // Output first ascent info.
        if (!empty($view->result[$row_count]->field_field_ascent[0]['rendered'])) {
          echo $view->result[$row_count]->field_field_ascent[0]['rendered']['#markup'];
        }
        ?></em></div>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>