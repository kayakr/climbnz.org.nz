<?php

/**
 * @file
 * Stub file for bootstrap_breadcrumb().
 */

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param array $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_breadcrumb()
 *
 * @ingroup theme_functions
 */
function climbnztheme_breadcrumb(array $variables) {
  // Use the Path Breadcrumbs theme function if it should be used instead.
  if (_bootstrap_use_path_breadcrumbs()) {
    return path_breadcrumbs_breadcrumb($variables);
  }

  $output = '';
  $breadcrumb = $variables['breadcrumb'];

  // Determine if we are to display the breadcrumb.
  $bootstrap_breadcrumb = bootstrap_setting('breadcrumb');
  if (($bootstrap_breadcrumb == 1 || ($bootstrap_breadcrumb == 2 && arg(0) == 'admin')) && !empty($breadcrumb)) {
    // Show home icon instead of text.
    if (strpos($breadcrumb[0], 'Home') !== FALSE) {
      $breadcrumb[0] = str_replace('Home', '<span class="glyphicon glyphicon-home" aria-hidden="true"></span>', $breadcrumb[0]);
    }

    $build = array(
      '#theme' => 'item_list__breadcrumb',
      '#attributes' => array(
        'class' => array('breadcrumb'),
      ),
      '#items' => $breadcrumb,
      '#type' => 'ol',
    );
    
    $output = drupal_render($build);
  }
  return $output;
}
