(function ($) {

  // Reduce visual impact of placeholder stars.
  Drupal.behaviors.climbnzRouteTableStars = {
    attach: function (context, settings) {
      $('.star').has('.off').css({'opacity': '.1'});
    }
  };

})(jQuery);
