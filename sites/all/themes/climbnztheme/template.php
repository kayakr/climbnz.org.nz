<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

/**
 * Copied from bootstrap_preprocess_html
 * Implements hook_preprocess_html().
 *
 * @param array $variables
 */
function climbnztheme_preprocess_html(array &$variables) {
  drupal_add_js(drupal_get_path('theme', 'climbnztheme') . '/js/bootstrap-toolkit.min.js');
}

/**
 * Overrides template_preprocess_page().
 * @param $variables
 */
function climbnztheme_preprocess_page(&$variables) {

  // Suggest page--node--place.tpl.php, etc.
  if (isset($variables['node']) && $variables['node']->type != '') {
    $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
  }

  // If we are showing page for Place, show breadcrumbs locating Place in hierarchy.
  if (isset($variables['node']) && $variables['node']->type == 'place') {
    // Add some javascript for tweaking star display on Route table.
    drupal_add_js(drupal_get_path('theme', 'climbnztheme') . '/js/route-table-stars.js');

    $variables['routes_count'] = ' ('. place_descendant_count($variables['node']->nid) .')';

    if (function_exists('nodehierarchy_get_breadcrumbs')) {
      $breadcrumb_links = nodehierarchy_get_breadcrumbs($variables['node']->nid);
      $breadcrumbs = '';
      if (count($breadcrumb_links) > 1) {
        array_shift($breadcrumb_links); // Drop first breadcrumb, as it will be rendered anyway.
        foreach ($breadcrumb_links as $breadcrumb) {
          // Add Home to breadcrumb.
          array_unshift($breadcrumb, l('Home', '<front>'));
          // Discard last item in breadcrumb.
          $discard = array_pop($breadcrumb);
          $breadcrumbs .= theme('breadcrumb', array('breadcrumb' => $breadcrumb));
        }
        $variables['breadcrumbs'] = $breadcrumbs;
      }
    }

    // Show type of Place after heading.
    $variables['place_type'] = '(' . check_plain($variables['node']->field_type[LANGUAGE_NONE][0]['value']) . ')';
  } // if place.
}

/**
 * Override template_preprocess_block().
 * @param $variables
 */
function climbnz_preprocess_block(&$variables) {

  switch ($variables['block']->bid) {
    case 'climbnz-climbnz_d7_eol':
      $variables['classes_array'][] = 'alert alert-warning';
      break;

    case 'climbnz-climbnz_intro':
    case 'climbnz_homepage-climbnz_hero_image':
      $variables['classes_array'][] = 'col-xs-12 col-sm-6';
      break;

    case 'comment-recent':
    case 'user-new':
    case 'user-online':
      $variables['classes_array'][] = 'col-xs-12 col-md-4';
      break;

    case 'views-climbnz_activity-block_1':
    $variables['classes_array'][] = 'col-xs-12 col-md-8';
      break;

    case 'climbnz_theme-climbnz_catalyst_cloud':
      $variables['classes_array'][] = 'col-xs-12 col-sm-3';
      break;
  }
}


/**
 * Override theme_form_element().
 * @param $variables
 *
 * @return string
 * @throws \Exception
 */
/*function climbnztheme_form_element($variables) {
  $element = &$variables['element'];

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}*/

/**
 * Override theme_links().
 *
 * @param $variables
 *
 * @return string
 */
function climbnztheme_links($variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);

      // climbnz
      if (strpos($key, 'comment-') !== FALSE) {
        $class[] = 'btn';
        $class[] = 'btn-default';
      }

      // Add first, last and active classes to the list of links to help out
      // themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
        && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for
        // adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}


/**
 * Override theme_nodehierarchy_new_child_links() to add Bootstrap classes to links.
 *
 * Display links to create new children nodes of the given node
 */
function climbnztheme_nodehierarchy_new_child_links($variables) {
  $node = $variables['node'];
  $out = array();
  $create_links = array();

  if (user_access('create child nodes') && (user_access('create child of any parent') || node_access('update', $node))) {
    foreach (nodehierarchy_get_allowed_child_types($node->type) as $key) {
      if (node_access('create', $key)) {
        $type_name = node_type_get_name($key);
        $destination = (array)drupal_get_destination() + array('parent' => $node->nid);
        $key = str_replace('_', '-', $key);
        $title = t('Add a new %s.', array('%s' => $type_name));
        // add bootstrap classes.s
        $create_links[] = l($type_name, "node/add/$key", array('query' => $destination, 'attributes' => array('title' => $title, 'class' => array('btn', 'btn-success'))));
      }
    }
    if ($create_links) {
      $out[] = array('#children' => '<div class="newchild">' . t("Create new child !s", array('!s' => implode(" | ", $create_links))) . '</div>');
    }
  }
  return $out;
}

/**
 * Override THEMENAME_menu_link__MENU_NAME() to add destination to login link.
 */
function climbnztheme_menu_link__user_menu(&$variables) {
  // If user is not on homepage, redirect login back to current page.
  if ($variables['element']['#href'] == 'user/login' && $_SERVER['REQUEST_URI'] != '/') {
    // @todo: check for existing destination parameter...?
    $variables['element']['#localized_options']['query'] = array('destination' => substr($_SERVER['REQUEST_URI'], 1));
  }

  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
