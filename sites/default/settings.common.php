<?php
/**
 * @file
 *  Configuration for ClimbNZ.
 */

// Custom string overrides.
$conf['locale_custom_strings_en'][''] = array(
  'Request new password' => 'Forgot your password?',
  'You are not authorized to access this page.' => 'You are not authorised to access this page.<br/><br/>If you have an account, please login. If you do not have an account, you can <a href="/user/register">register an account</a>.',
  'Sorry, unrecognized username or password. <a href="@password">Have you forgotten your password?</a>' => 'Sorry, unrecognised email or password. <a href="@password">Have you forgotten your password?</a>',
  'You are not authorized to access this page.' => 'You are not authorised to access this page.',
);

ini_set('xdebug.max_nesting_level', 200);

$conf['site_frontpage'] = 'home';

// Force SSL when available.
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  $_SERVER['HTTPS'] = 'on';
}

// Put files shared between dev and prod environments in here.
if (!isset($environment)) {
  if (strpos($_SERVER['HTTP_HOST'], 'local') !== FALSE) {
    $environment = 'local';
  }
  elseif (strpos($_SERVER['HTTP_HOST'], 'dev.') !== FALSE) {
    $environment = 'development';
  }
  elseif (strpos($_SERVER['HTTP_HOST'], 'd7.climbnz.org.nz') !== FALSE) {
    $environment = 'staging';
  }
  elseif (strpos($_SERVER['HTTP_HOST'], 'staging-d7.climbnz.org.nz') !== FALSE) {
    $environment = 'staging-sitehost';
  }
  elseif (strpos($_SERVER['HTTP_HOST'], 'climbnz.catalystdemo.net.nz') !== FALSE) {
    $environment = 'staging-catalyst';
  }
  else {
    // Default to production.
    $environment = 'production';
  }
}

switch ($environment) {
  case 'production-sitehost':
    ini_set('display_errors', FALSE);
    ini_set('max_execution_time', 60);

    // c/- PreviousNext
    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'admin') === 0 || strpos($_GET['q'], 'en/admin') === 0)) {
      ini_set('memory_limit', '196M');
    }
    // Node edit pages are memory heavy too.
    if (isset($_GET['q']) && preg_match('@^node\/([0-9]+)\/edit$@', $_GET['q'])) {
      ini_set('memory_limit', '196M');
    }

    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'batch') === 0)) {
      ini_set('memory_limit', '196M');
    }

    $conf['file_directory_path'] = 'sites/default/files';

    // $conf['flog_enabled'] = TRUE;
    // $conf['flog_path'] = $conf['file_directory_path'];
    // $conf['flog_file'] = 'drupal-7eb632827cb0dc1f255b7d1957a07c9737ba2ce9.log';

    // Production is behind Varnish
    // $conf['reverse_proxy'] = TRUE;
    // $conf['reverse_proxy_addresses'] = array('127.0.0.1');
    // $conf['reverse_proxy_header'] = 'HTTP_X_FORWARDED_FOR';
    // $conf['block_cache'] = FALSE;
    $conf['cache_lifetime'] = '3600';
    $conf['page_cache_maximum_age'] = '3600';
    // Uncomment this when varnish is working, this prevents double caching of the page.
    $conf['cache_class_cache_page'] = 'DrupalFakeCache';

    // Connection details for Catalyst Squid proxies.
    // $conf['proxy_server'] = 'ext-proxy-prod.catalyst.net.nz';
    // $conf['proxy_port'] = '3128';
    // $conf['proxy_exceptions'] = array('cat-chcsc-prod-solr1.servers.catalyst.net.nz');
    break;

  case 'production':
    ini_set('display_errors', FALSE);
    ini_set('max_execution_time', 60);

    // c/- PreviousNext
    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'admin') === 0 || strpos($_GET['q'], 'en/admin') === 0)) {
      ini_set('memory_limit', '196M');
    }
    // Node edit pages are memory heavy too.
    if (isset($_GET['q']) && preg_match('@^node\/([0-9]+)\/edit$@', $_GET['q'])) {
      ini_set('memory_limit', '196M');
    }

    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'batch') === 0)) {
      ini_set('memory_limit', '196M');
    }

    $conf['file_directory_path'] = 'sites/default/files';

    $conf['flog_enabled'] = TRUE;
    $conf['flog_path'] = $conf['file_directory_path'];
    $conf['flog_file'] = 'drupal-7eb632827cb0dc1f255b7d1957a07c9737ba2ce9.log';

    // Production is behind Varnish
    $conf['reverse_proxy'] = TRUE;
    $conf['reverse_proxy_addresses'] = array('127.0.0.1');
    $conf['reverse_proxy_header'] = 'HTTP_X_FORWARDED_FOR';
    $conf['block_cache'] = FALSE;
    $conf['cache_lifetime'] = '3600';
    $conf['page_cache_maximum_age'] = '3600';
    // Uncomment this when varnish is working, this prevents double caching of the page.
    $conf['cache_class_cache_page'] = 'DrupalFakeCache';

    // Connection details for Catalyst Squid proxies.
    $conf['proxy_server'] = 'ext-proxy-prod.catalyst.net.nz';
    $conf['proxy_port'] = '3128';
    $conf['proxy_exceptions'] = array('cat-chcsc-prod-solr1.servers.catalyst.net.nz');
    break;

  case 'staging-sitehost':
    ini_set('error_reporting', E_ALL); // ^E_WARNING
    ini_set('display_errors', TRUE);
    ini_set('max_execution_time', 60);

    // c/- PreviousNext
    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'admin') === 0 || strpos($_GET['q'], 'en/admin') === 0)) {
      ini_set('memory_limit', '196M');
    }
    // Node edit pages are memory heavy too.
    if (isset($_GET['q']) && preg_match('@^node\/([0-9]+)\/edit$@', $_GET['q'])) {
      ini_set('memory_limit', '196M');
    }

    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'batch') === 0)) {
      ini_set('memory_limit', '196M');
    }

    $conf['flog_enabled'] = TRUE;
    $conf['flog_path'] = $conf['file_directory_path'];
    //$conf['flog_file'] = 'drupal-7eb632827cb0dc1f255b7d1957a07c9737ba2ce9.log';

    // File path, used in migrate_climbnz/migrate_climbnz.migrate.inc
    // sites/climbnz.org.nz/files gets appended.
    // $conf['migrate_climbnz_source_dir'] = '/home/nzac/climbnz.org.nz/';

    // Configure environment module.
    $conf['environment_indicator_overwrite'] = TRUE;
    $conf['environment_indicator_overwritten_name'] = 'STAGING';
    $conf['environment_indicator_overwritten_color'] = '#fa630a'; // orange
    $conf['environment_indicator_overwritten_drawer_color'] = '#333333';
    $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
    $conf['environment_indicator_overwritten_position'] = 'top';
    $conf['environment_indicator_overwritten_fixed'] = FALSE;
    break;

  case 'staging-catalyst':
    ini_set('error_reporting', E_ALL); // ^E_WARNING
    ini_set('display_errors', TRUE);
    ini_set('max_execution_time', 60);

    // c/- PreviousNext
    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'admin') === 0 || strpos($_GET['q'], 'en/admin') === 0)) {
      ini_set('memory_limit', '196M');
    }
    // Node edit pages are memory heavy too.
    if (isset($_GET['q']) && preg_match('@^node\/([0-9]+)\/edit$@', $_GET['q'])) {
      ini_set('memory_limit', '196M');
    }

    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'batch') === 0)) {
      ini_set('memory_limit', '196M');
    }

    // Each element of this array is the IP address of any of your reverse proxies.
    $conf['reverse_proxy_addresses'] = array('127.0.0.1');

    // Connection details for Catalyst Squid proxies.
    $conf['proxy_server'] = 'ext-proxy-staging.catalyst.net.nz';
    $conf['proxy_port'] = '3128';
    $conf['proxy_exceptions'] = array('cat-chcsc-staging-solr.servers.catalyst.net.nz');

    $conf['omit_vary_cookie'] = true;

    $conf['file_directory_path'] = 'sites/default/files';
    //$conf['file_public_path'] = $conf['file_directory_path'];

    $conf['flog_enabled'] = TRUE;
    $conf['flog_path'] = $conf['file_directory_path'];
    //$conf['flog_file'] = 'drupal-7eb632827cb0dc1f255b7d1957a07c9737ba2ce9.log';

    // File path, used in migrate_climbnz/migrate_climbnz.migrate.inc
    // sites/climbnz.org.nz/files gets appended.
    $conf['migrate_climbnz_source_dir'] = '/home/nzac/climbnz.org.nz/';

    // Configure environment module.
    $conf['environment_indicator_overwrite'] = TRUE;
    $conf['environment_indicator_overwritten_name'] = 'STAGING';
    $conf['environment_indicator_overwritten_color'] = '#fa630a'; // orange
    $conf['environment_indicator_overwritten_drawer_color'] = '#333333';
    $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
    $conf['environment_indicator_overwritten_position'] = 'top';
    $conf['environment_indicator_overwritten_fixed'] = FALSE;
    break;

  case 'staging': // Simplehost
    ini_set('error_reporting', E_ALL); // ^E_WARNING
    ini_set('display_errors', TRUE);
    ini_set('max_execution_time', 60);

    // c/- PreviousNext
    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'admin') === 0 || strpos($_GET['q'], 'en/admin') === 0)) {
      ini_set('memory_limit', '196M');
    }
    // Node edit pages are memory heavy too.
    if (isset($_GET['q']) && preg_match('@^node\/([0-9]+)\/edit$@', $_GET['q'])) {
      ini_set('memory_limit', '196M');
    }

    // Memory allocation to be 256MB. This is to cover cron etc.
    if (isset($_GET['q']) && (strpos($_GET['q'], 'batch') === 0)) {
      ini_set('memory_limit', '196M');
    }

    $conf['file_directory_path'] = 'sites/default/files';

    $conf['flog_enabled'] = TRUE;
    $conf['flog_path'] = $conf['file_directory_path'];
    $conf['flog_file'] = 'drupal-7eb632827cb0dc1f255b7d1957a07c9737ba2ce9.log';

    // File path, used in migrate_climbnz/migrate_climbnz.migrate.inc
    // sites/climbnz.org.nz/files gets appended.
    $conf['migrate_climbnz_source_dir'] = '/home/nzac/climbnz.org.nz/';

    // Configure environment module.
    $conf['environment_indicator_overwrite'] = TRUE;
    $conf['environment_indicator_overwritten_name'] = 'STAGING';
    $conf['environment_indicator_overwritten_color'] = '#fa630a'; // orange
    $conf['environment_indicator_overwritten_drawer_color'] = '#333333';
    $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
    $conf['environment_indicator_overwritten_position'] = 'top';
    $conf['environment_indicator_overwritten_fixed'] = FALSE;
    break;

  case 'development':
  case 'local':
    ini_set('error_reporting', E_ALL); // ^E_WARNING
    ini_set('display_errors', TRUE);
    ini_set('max_execution_time', 60);

    $conf['file_directory_path'] = 'sites/default/files';

    $conf['flog_enabled'] = TRUE;
    $conf['flog_path'] = $conf['file_directory_path'];
    $conf['flog_file'] = 'drupal.log';

    // Source file path, used in migrate_climbnz/migrate_climbnz.migrate.inc
    $conf['migrate_climbnz_source_dir'] = '/Users/jonathan/Sites/climbnz.local/';

    // Configure environment module.
    $conf['environment_indicator_overwrite'] = TRUE;
    $conf['environment_indicator_overwritten_name'] = 'DEVELOPMENT';
    $conf['environment_indicator_overwritten_color'] = '#24ae43';
    $conf['environment_indicator_overwritten_drawer_color'] = '#333333';
    $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
    $conf['environment_indicator_overwritten_position'] = 'top';
    $conf['environment_indicator_overwritten_fixed'] = FALSE;
    break;
}

