## climbnz.org.nz on Drupal 7

* Code: https://gitlab.com/kayakr/climbnz.org.nz
* Database: c/- jhunt
* Public files: download from

## Lando
1. `git clone https://gitlab.com/kayakr/climbnz.org.nz`
1. `cd climbnz.org.nz`
1. `cp sites/default/settings.php sites/default/settings.php`
1. Edit `sites/default/settings.php` to access database, set environment, and include `settings.common.php`:
```php
$databases = array (
  'default' =>
    array (
      'default' =>
        array (
          'database' => 'drupal7',
          'username' => 'drupal7',
          'password' => 'drupal7',
          'host' => 'database',
          'port' => '',
          'driver' => 'mysql',
          'prefix' => '',
          'charset' => 'utf8mb4',
          'collation' => 'utf8mb4_unicode_ci',
        ),
    ),
);

$environment = 'local';

// Include local settings file if present
if (file_exists(dirname(__FILE__) . '/settings.common.php')) {
  require_once(dirname(__FILE__) . '/settings.common.php');
}
```
5. `lando start`
6. `lando db-import drupal-site-climbnz_YYYYMMDD.sql`

## Maintenance
After updating Drupal core, apply patches to menu system to cope with Place menu hierarchy.
1. `patch -p1 <_menu_build_tree_avoid_place_menu_links.patch`
1. `patch -p1 <_menu_get_options_avoid_place_menu.patch`

